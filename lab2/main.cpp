#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <exception>
#include <map>
#include "parser.h"
#include "WorkFlow.h"
#include "exceptions.h"

using std::string;
using std::pair;
using std::map;

int main(int argc, char **argv) {
    const int MINIMAL_NEEDED_ARGC = 2;
    string inputFileName, outputFileName;
    std::ifstream fileStream;
    workflow::Parser *parser = nullptr;
    workflow::Executor executor(new workflow::BlockCreator);

    try {

        if (argc < MINIMAL_NEEDED_ARGC) {
            throw std::invalid_argument("Too few arguments");
        }
        map<string, string *> arguments = {
                pair<string, string *>("-i", &inputFileName),
                pair<string, string *>("-o", &outputFileName)
        };

        for (int i = 1; i < argc; i++) {
            if (arguments.find(argv[i]) == arguments.end()) {
                std::string name = argv[i];
                fileStream.open(name);
                if (!fileStream.is_open()) {
                    throw workflow::exceptions::WorkflowFileNotFound(name);
                }
                parser = new workflow::Parser(fileStream);

            } else if (argc > i + 1) {
                *(arguments[argv[i]]) = argv[i + 1];
                i++;
            }
        }

        if (parser == nullptr) {
            throw std::invalid_argument("no program name");
        }
        executor.execute(parser, inputFileName, outputFileName);

        std::cout << "Execution finished without exceptions.\n";
    }
    catch (std::exception &e) {
        std::cout << e.what() << std::endl;
    }
    if (fileStream.is_open()) {
        fileStream.close();
    }

    delete parser;

    return 0;
}
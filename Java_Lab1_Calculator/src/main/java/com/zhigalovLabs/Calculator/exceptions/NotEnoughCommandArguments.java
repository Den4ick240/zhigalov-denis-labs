package com.zhigalovLabs.Calculator.exceptions;

public class NotEnoughCommandArguments extends CalculatorException {
    @Override
    public String getMessage() {
        return "Not enough command arguments";
    }
}

package Commands;

import com.zhigalovLabs.Calculator.CommandContext;
import com.zhigalovLabs.Calculator.commands.CommandArgumentSetter;
import com.zhigalovLabs.Calculator.commands.DefineCommand;
import com.zhigalovLabs.Calculator.commands.ICommand;
import com.zhigalovLabs.Calculator.exceptions.CalculatorException;
import com.zhigalovLabs.Calculator.exceptions.DigitAsAParameter;
import org.junit.Test;

public class DefineCommandTest {
    @Test(expected = DigitAsAParameter.class)
    public void DigitAsAParameter() throws CalculatorException {
        String[] args = { "DEFINE", "2.5", "2" };
        CommandArgumentSetter command = new DefineCommand();
        command.setArguments(args);
        ((ICommand)command).run(new CommandContext());
    }

}

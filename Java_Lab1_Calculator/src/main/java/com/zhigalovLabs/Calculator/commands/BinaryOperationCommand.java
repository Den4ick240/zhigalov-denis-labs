package com.zhigalovLabs.Calculator.commands;

import com.zhigalovLabs.Calculator.CommandContext;

import java.util.EmptyStackException;

public abstract class BinaryOperationCommand extends CommandArgumentSetter implements ICommand {
    public BinaryOperationCommand(int argc) {
        super(argc);
    }

    public abstract double operation(double a, double b);

    @Override
    public void run(CommandContext context) throws EmptyStackException {
        double a, b;
        b = context.stackPop();
        try {
            a = context.stackPop();
        } catch (EmptyStackException e) {
            context.stackPush(b);
            throw e;
        }
        a = operation(a, b);
        context.stackPush(a);
    }
}

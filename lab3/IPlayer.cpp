#include "IPlayer.h"

namespace sea_battle {

    IPlayer::IPlayer(OwnField &ownField, EnemyField &enemyField)
            : _ownField(ownField), _enemyField(enemyField) {
    }

}

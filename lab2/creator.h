
#include <map>
#include <functional>
#include "worker.h"


#pragma once

namespace workflow {
    class IBlockCreator {
    public:
        virtual IBlock *CreateBlock(int index, const std::string &blockName, const std::vector<std::string> &args) = 0;
    };

    class BlockCreator : public IBlockCreator {
    public:
        IBlock *CreateBlock(int index, const std::string &blockName, const std::vector<std::string> &args) override;

    protected:
        std::map<std::string, std::pair<int, std::function<IBlock *(
                const std::vector<std::string> &args)>>> validation_map = {
                {"readfile",  {1, [](const std::vector<std::string> &args) {
                    return new Readfile(args[0]);
                }}},
                {"writefile", {1, [](const std::vector<std::string> &args) {
                    return new Writefile(args[0]);
                }}},
                {"sort",      {0, [](const std::vector<std::string> &args) {
                    return new Sort();
                }}},
                {"replace",   {2, [](const std::vector<std::string> &args) {
                    return new Replace(args[0], args[1]);
                }}},
                {"grep",      {1, [](const std::vector<std::string> &args) {
                    return new Grep(args[0]);
                }}},
                {"dump",      {1, [](const std::vector<std::string> &args) {
                    return new Dump(args[0]);
                }}}
        };
    };
}
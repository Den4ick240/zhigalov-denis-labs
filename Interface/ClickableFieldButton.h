#pragma once

#include <QPushButton>
#include <QMouseEvent>
#include "InterfaceConsts.h"

namespace qt_interface {

    class ClickableFieldButton : public QPushButton
    {
        Q_OBJECT
    public:
        ClickableFieldButton(int x, int y);

    private:
        int onField_x, onField_y;

    private slots:
        void mousePressEvent(QMouseEvent *e);

    signals:
        void Clicked(Qt::MouseButton button, int x, int y);

    };
}

#include "TritSetData.h"
#include <string>
#include <algorithm>
#include <map>

namespace tritset
{
	TritSetData::TritSetData(size_t size, Trit val)
		:trit_num(size)
	{
		arr_size = unit_pos(size - 1) + 1;
		arr = new unit[arr_size];

		memset(arr, INIT_VALUES_FOR_UNITS.at(val), arr_size * sizeof(unit));

		arr[arr_size - 1] &= ~(~unit(0) << trit_pos(size));		//clear unused trits
		arr[arr_size - 1] |= (UNKNOWN_UNIT << trit_pos(size));	//set Unknown value to unused bits
	}

	TritSetData::~TritSetData()
	{
		delete[] arr;
	}

	void TritSetData::resize(size_t new_size)
	{
		unit *buff;
		size_t new_arr_size = unit_pos(new_size - 1) + 1;


		if (new_arr_size == arr_size)
		{
			for (size_t i = new_size; i < trit_num; i++)
				setTrit(i, Unknown);
			trit_num = new_size;
			return;
		}

		trit_num = new_size;
		buff = new unit[new_arr_size];
		memcpy(buff, arr, std::min(new_arr_size, arr_size));

		for (size_t i = arr_size; i < new_arr_size; i++)
			buff[i] = UNKNOWN_UNIT;

		arr_size = new_arr_size;

		delete[] arr;
		arr = buff;
	}

	Trit TritSetData::getTrit(size_t i) const
	{
		if (i < trit_num)
		{
			unit u = arr[unit_pos(i)];
			u >>= trit_pos(i);
			u &= TWO_BITS_TRUE_MASK;
			return Trit(u);
		}
		else
			return Unknown;
	}

	Trit TritSetData::setTrit(size_t i, Trit val)
	{
		if (i >= trit_num)
			if (val != True && val != False)
				return val;
			else
				resize(i + 1);

		arr[unit_pos(i)] &= ~(TWO_BITS_TRUE_MASK << trit_pos(i));			//clear required trit.
		arr[unit_pos(i)] |= unit(val) << trit_pos(i);						//set the value to required position

		return val;
	}


	TritSetData & TritSetData::operator&=(const TritSetData & obj)
	{
		this->resize(std::max((this->trit_num), (obj.trit_num)));

		for (size_t i = 0; i < obj.arr_size; i++)
			this->arr[i] &= obj.arr[i];

		for (size_t i = obj.arr_size; i < this->arr_size; i++)
			this->arr[i] &= UNKNOWN_UNIT;

		return *this;
	}

	TritSetData & TritSetData::operator|=(const TritSetData & obj)
	{
		this->resize(std::max(this->trit_num, obj.trit_num));

		for (size_t i = 0; i < obj.arr_size; i++)
			this->arr[i] |= obj.arr[i];

		for (size_t i = obj.arr_size; i < this->arr_size; i++)
			this->arr[i] |= UNKNOWN_UNIT;

		return *this;
	}

	TritSetData & TritSetData::operator^=(const TritSetData & obj)
	{
		this->resize(std::max(this->trit_num, obj.trit_num));

		for (size_t i = 0; i < obj.arr_size; i++)
		{
			unit mask = get_unknown_mask(this->arr[i]) & get_unknown_mask(obj.arr[i]);
			this->arr[i] ^= obj.arr[i];
			this->arr[i] &= mask;
			this->arr[i] |= ~(mask)& UNKNOWN_UNIT;
		}

		for (size_t i = obj.arr_size; i < this->arr_size; i++)
			this->arr[i] = UNKNOWN_UNIT;

		return *this;
	}

	TritSetData & TritSetData::flip()
	{
		for (size_t i = 0; i < arr_size; i++)
		{
			unit mask = get_unknown_mask(arr[i]);
			arr[i] = ~arr[i];
			arr[i] &= mask;
			arr[i] |= ~(mask)& UNKNOWN_UNIT;
		}
		return *this;
	}

	int TritSetData::trit_pos(int i)
	{
		return	(i * 2) % (8 * sizeof(unit));
	}

	size_t TritSetData::unit_pos(int i)
	{
		return i / (sizeof(unit) * 4);
	}

	unit TritSetData::get_unknown_mask(unit a)
	{
		unit mask = a ^ (a >> 1);
		mask &= UNKNOWN_UNIT;
		mask |= mask << 1;
		return ~mask;
	}
}
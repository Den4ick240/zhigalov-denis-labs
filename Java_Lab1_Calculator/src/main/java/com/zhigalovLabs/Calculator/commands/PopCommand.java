package com.zhigalovLabs.Calculator.commands;

import com.zhigalovLabs.Calculator.CommandContext;

public class PopCommand extends CommandArgumentSetter implements ICommand {
    public PopCommand() {
        super(0);
    }
    @Override
    public void run(CommandContext context) {
        context.stackPop();
    }
}

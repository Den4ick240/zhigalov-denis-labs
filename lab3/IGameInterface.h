#pragma once

#include "Field.h"
#include <set>
#include <string>

namespace sea_battle {

    class IGameInterface {
    public:
        struct MoveStruct {
            int x, y;
            enum MoveType {
                exclude, shoot
            } moveType;
        };

        virtual Ship getNextShipToPlace(const std::map<int, int> &unplacedShips) = 0;

        virtual MoveStruct getNextMove(const OwnField &ownField, const EnemyField &enemyField) = 0;

        virtual std::string
        choosePlayerType(const std::set<std::string> &playerTypesSet, const std::string &playerName) = 0;
    };

}

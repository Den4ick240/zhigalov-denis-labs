package ru.nsu.ccfit.zhigalov.network.messages;

import ru.nsu.ccfit.zhigalov.network.TextMessage;
import ru.nsu.ccfit.zhigalov.network.messages.IMessage;

public class NewTextMessage extends IMessage {
    public NewTextMessage(TextMessage message) {
        this.message = message;
    }

    public TextMessage getMessage() {
        return message;
    }

    TextMessage message;
}

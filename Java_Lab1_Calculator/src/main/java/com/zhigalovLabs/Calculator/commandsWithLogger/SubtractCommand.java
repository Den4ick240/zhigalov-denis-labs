package com.zhigalovLabs.Calculator.commandsWithLogger;

public class SubtractCommand extends BinaryOperationCommand {
    public SubtractCommand() {
        super(0);
    }

    @Override
    public double operation(double a, double b) {
        return a - b;
    }
}

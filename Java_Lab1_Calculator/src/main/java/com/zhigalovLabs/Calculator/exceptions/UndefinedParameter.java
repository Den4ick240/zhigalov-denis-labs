package com.zhigalovLabs.Calculator.exceptions;

public class UndefinedParameter extends CalculatorException {
    private final String name;

    @Override
    public String getMessage() {
        return "Undefined parameter: " + name;
    }

    public UndefinedParameter(String name) {
        this.name = name;
    }
}

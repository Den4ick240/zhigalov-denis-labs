package com.zhigalovLabs.Calculator.commandsWithLogger;

import com.zhigalovLabs.Calculator.CommandContext;
import com.zhigalovLabs.Calculator.commands.ICommand;

import java.util.EmptyStackException;

public class SqrtCommand extends CommandArgumentSetter implements ICommand {
    public SqrtCommand() {
        super(0);
    }

    @Override
    public void run(CommandContext context) throws EmptyStackException {
        double val, result;
        val = context.stackPop();
        result = Math.sqrt(val);
        context.stackPush(result);
        logger.info(String.format("Square root of %f; Result: %f", val, result));
    }
}

package com.zhigalovLabs.Calculator.commands;

import com.zhigalovLabs.Calculator.CommandContext;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.EmptyStackException;

public class PrintCommand extends CommandArgumentSetter implements ICommand {
    public PrintCommand() {
        super(0);
    }
    @Override
    public void run(CommandContext context) throws EmptyStackException {
        System.out.println(context.stackPeek());
    }
}

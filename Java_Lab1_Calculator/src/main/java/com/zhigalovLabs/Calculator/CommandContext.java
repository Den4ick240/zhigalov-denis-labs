package com.zhigalovLabs.Calculator;

import com.zhigalovLabs.Calculator.exceptions.UndefinedParameter;

import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import java.util.logging.Logger;

public class CommandContext {
    static private Logger logger = Main.getLogger();
    private Map<String, Double> parameters;
    private Stack<Double> stack;

    public CommandContext() {
        parameters = new HashMap<String, Double>();
        stack = new Stack<Double>();
    }

    public void setParameter(String name, Double value) {
        parameters.put(name, value);
        logger.info(String.format("New parameter was defined: %f as %s", value, name));
    }

    public Double getParameter(String name) throws UndefinedParameter {
        Double out = parameters.get(name);
        if (out == null) {
            var e = new UndefinedParameter(name);
            logger.warning(e.getMessage());
            throw e;
        }
        logger.info(String.format("Parameter requested: %f as %s", out, name));
        return out;
    }

    public void stackPush(Double val) {
        stack.push(val);
        logger.info("Value pushed to stack:" + Double.toString(val));
    }

    public Double stackPop() throws EmptyStackException {
        double val = stack.pop();
        logger.info("Value popped from stack:" + Double.toString(val));
        return val;
    }

    public Double stackPeek() throws EmptyStackException {
        double val = stack.peek();
        logger.info("Value peeked from stack:" + Double.toString(val));
        return val;
    }
}

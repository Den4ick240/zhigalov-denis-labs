#pragma once
#include <ClickableField.h>
#include <GameScene.h>
#include "../lab3/IGameInterface.h"


namespace qt_interface {
    class BattleScene : public GameScene {
        Q_OBJECT
    public:
        typedef GraphicsField<sea_battle::OwnField> GraphicsOwnFieldType;
        typedef ClickableField<sea_battle::EnemyField> GraphicsEnemyFieldType;

        BattleScene(const sea_battle::OwnField *ownField, const sea_battle::EnemyField *enemyField);

        sea_battle::IGameInterface::MoveStruct getMove();

        void refresh();

        void endGame(const std::string &winnerName, int winnerId);

        void connectToExitButton(QObject *obj, const char* slot);

    signals:
        void moveDoneSignal();

    private slots:
        void moveHandler(Qt::MouseButton button, int x, int y);

    private:
        const sea_battle::OwnField *_ownField;
        const sea_battle::EnemyField *_enemyField;
        GraphicsOwnFieldType *_graphicsOwnField;
        GraphicsEnemyFieldType *_graphicsEnemyField;
        sea_battle::IGameInterface::MoveStruct _lastMove;
        QLabel *_winnerLabel;
        QPushButton *_exitButton;

        constexpr static const double FIELD_SIZE_RATIO = 0.75;
        constexpr static const double EXIT_BTN_HEIGHT_RATIO = 0.1;
        constexpr static const double EXIT_BTN_WITHS_TO_HEIGHT_RATIO = 2;
        constexpr static const double EXIT_BTN_OFFSET_RATIO = 0.1;
    };
}

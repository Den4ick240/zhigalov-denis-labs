QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    ../lab3/Field.cpp \
    ../lab3/Game.cpp \
    ../lab3/IGameInterface.cpp \
    ../lab3/IPlayer.cpp \
    ../lab3/ManualPlayer.cpp \
    ../lab3/RandomBotPlayer.cpp \
    ../lab3/Ship.cpp \
    ../lab3/SmartBotPlayer.cpp \
    BattleScene.cpp \
    ClickableFieldButton.cpp \
    GameInterface.cpp \
    GameQt.cpp \
    SelectionMenu.cpp \
    ShipPlacingScene.cpp \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    ../lab3/Consts.h \
    ../lab3/Field.h \
    ../lab3/Game.h \
    ../lab3/IGameInterface.h \
    ../lab3/IPlayer.h \
    ../lab3/ManualPlayer.h \
    ../lab3/RandomBotPlayer.h \
    ../lab3/Ship.h \
    ../lab3/SmartBotPlayer.h \
    BattleScene.h \
    ClickableField.h \
    ClickableFieldButton.h \
    GameInterface.h \
    GameQt.h \
    GameScene.h \
    GraphicsField.h \
    InterfaceConsts.h \
    SelectionMenu.h \
    ShipPlacingScene.h \
    mainwindow.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES = \
    resourses.qrc

DISTFILES +=

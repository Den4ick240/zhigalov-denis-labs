package com.zhigalovLabs.Calculator.exceptions;

public class CommandLoading extends ConfigFileException {
    final String name;

    public CommandLoading(String name) {
        this.name = name;
    }

    @Override
    public String getMessage() {
        return "Couldn't load class " + name;
    }
}

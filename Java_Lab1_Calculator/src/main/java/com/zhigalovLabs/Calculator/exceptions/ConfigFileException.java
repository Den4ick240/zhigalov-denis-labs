package com.zhigalovLabs.Calculator.exceptions;

public class ConfigFileException extends CalculatorException {
    @Override
    public String getMessage() {
        return "Config file error";
    }
}

#include "creator.h"
#include "exceptions.h"

namespace workflow {

    IBlock *BlockCreator::CreateBlock(int index, const std::string &blockName, const std::vector<std::string> &args) {
        auto creator = validation_map.find(blockName);
        if (creator == validation_map.end()) {
            throw exceptions::BlockType(index, blockName);
        }
        int neededAmount = creator->second.first;
        int actualAmount = args.size();
        if (neededAmount != actualAmount) {
            throw exceptions::ArgsAmount(index, blockName, neededAmount, actualAmount);
        }

        return creator->second.second(args);
    }
}
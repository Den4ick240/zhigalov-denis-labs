#pragma once

#include <string>



namespace workflow {
    const std::string OPEN_KEY_WORD("desc");
    const std::string CLOSE_KEY_WORD("csed");
    const std::string CHAIN_KEY_WORD("->");
    const std::string BLOCK_DEF_KEY_WORD("=");
    const std::string EOF_MESSAGE("end of file");
    const std::string BLOCK_ID("block id");
}
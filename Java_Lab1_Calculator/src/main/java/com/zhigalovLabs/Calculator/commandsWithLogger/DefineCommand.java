package com.zhigalovLabs.Calculator.commandsWithLogger;

import com.zhigalovLabs.Calculator.CommandContext;
import com.zhigalovLabs.Calculator.commands.ICommand;
import com.zhigalovLabs.Calculator.exceptions.DigitAsAParameter;
import com.zhigalovLabs.Calculator.exceptions.CalculatorException;

public class DefineCommand extends CommandArgumentSetter implements ICommand {
    public DefineCommand() {
        super(2);
    }

    @Override
    public void run(CommandContext context) throws CalculatorException {
        try {
            Double.parseDouble(getArgs()[1]);
            var e = new DigitAsAParameter();
            logger.warning(e.getMessage());
            throw e;
        } catch (NumberFormatException ignored) {}

        double val;
        try {
            val = Double.parseDouble(getArgs()[2]);
        } catch (NumberFormatException e) {
            val = context.getParameter(getArgs()[2]);
        }
        logger.info(String.format("Parameter defined: %f as %s", val, getArgs()[1]));
        context.setParameter(getArgs()[1], val);
    }
}

#include <iostream>
#include <string>
#include <fstream>
#include "Reader.h"
#include "Writer.h"

using std::string;
using std::cout;
using std::endl;

int main(/*int argc, char ** argv*/)
{
	Zhigalov::Reader reader;
	Zhigalov::Writer writer;

	int argc = 3;
	const char argv[3][16] = { "hd", "input.txt", "output.txt" };

	try
	{
		if (argc < 3)
		{
			throw std::exception("Wrong amount of arguments. Two arguments are needed.");
		}
		reader.open_file(argv[1]);
		reader.read_words();
		writer.open_file(argv[2]);
		writer.count_words(reader.get_word_map());
		writer.write_words();
	}
	catch (std::exception & e)
	{
		cout << e.what() << endl;
		system("pause");
	}
	return 0;
}
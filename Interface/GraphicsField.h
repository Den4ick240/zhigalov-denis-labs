#pragma once
#include <InterfaceConsts.h>
#include "../lab3/Field.h"

namespace qt_interface {

    enum class FieldRefreshMode {
        refreshAll,
        notRefreshShips
    };

    template<class fieldType>
    class GraphicsField : public QGraphicsPixmapItem {
    public:
        explicit GraphicsField (const fieldType *field, qreal size, qreal x = 0, qreal y = 0, FieldRefreshMode refreshMode = FieldRefreshMode::refreshAll);

        void addToScene(QGraphicsScene *scene);

        void setField(qreal x, qreal y, qreal size);

        void refresh();

        int cellSize() {
            return _cellSize;
        }

        void setRefreshMode(FieldRefreshMode mode) {
            _refreshMode = mode;
        }

        GraphicsField(const GraphicsField &) = delete;
        GraphicsField(GraphicsField &&) = delete;
        GraphicsField &operator=(const GraphicsField &) = delete;
        GraphicsField &operator=(GraphicsField &&) = delete;

    protected:
        const fieldType *_field;
        FieldRefreshMode _refreshMode;
        sea_battle::CellState _cellStateField[FIELD_CELLS_NUM][FIELD_CELLS_NUM];
        QGraphicsPixmapItem *_cellPixmapField[FIELD_CELLS_NUM][FIELD_CELLS_NUM];
        QGraphicsRectItem *_cellRectField[FIELD_CELLS_NUM][FIELD_CELLS_NUM];
        QGraphicsPixmapItem *_ships[sea_battle::SHIPS_AMOUNT];
        QLabel *_verticalSignature[FIELD_CELLS_NUM];
        QLabel *_horizontalSignature[FIELD_CELLS_NUM];
        int _cellSize;
        int _size;
        QFont _font = STANDART_FONT;
        constexpr static const qreal _labelSizeRatio = 0.8;

        void setShips();

        void setCells();

        void setSignature();

        std::pair<int, int> convertCellCoordFromLogicFieldToPixel(int x, int y);

        void foreachCell(std::function<void(QGraphicsPixmapItem *, int, int)> body);

    };

    template<class fieldType>
    GraphicsField<fieldType>::GraphicsField(const fieldType *field, qreal size, qreal x, qreal y, FieldRefreshMode refreshMode)
        : _field(field), _refreshMode(FieldRefreshMode::refreshAll)
    {
        foreachCell([this](QGraphicsPixmapItem *, int x, int y) {
           _cellRectField[y][x] = new QGraphicsRectItem(this);
        });

        for (int i = 0; i < FIELD_CELLS_NUM; i++) {
            _horizontalSignature[i] = new QLabel(FIELD_HORIZONTAL_SIGNATURE[i]);
            _verticalSignature[i] = new QLabel(FIELD_VERTICAL_SIGNATURE[i]);

            _horizontalSignature[i]->setAlignment(Qt::AlignCenter);
            _horizontalSignature[i]->setStyleSheet(ITEM_STYLE_SHEET);
            _verticalSignature[i]->setAlignment(Qt::AlignCenter);
            _verticalSignature[i]->setStyleSheet(ITEM_STYLE_SHEET);
        }

        for (auto &ship : _ships) {
            ship = new QGraphicsPixmapItem(this);
        }

        foreachCell([this](QGraphicsPixmapItem *, int x, int y) {
           _cellPixmapField[y][x] = new QGraphicsPixmapItem(this);
        });

        setField(x, y, size);
        refresh();
        _refreshMode = refreshMode;
    }

    template<class fieldType>
    void GraphicsField<fieldType>::addToScene(QGraphicsScene *scene)
    {
        scene->addItem(this);
        for (int i = 0; i < FIELD_CELLS_NUM; i++) {
            scene->addWidget(_horizontalSignature[i]);
            scene->addWidget(_verticalSignature[i]);
        }
    }

    template<class fieldType>
    void GraphicsField<fieldType>::setField(qreal new_x, qreal new_y, qreal size)
    {
        _size = size;
        _cellSize = size / (FIELD_CELLS_NUM + 1);

        setPos(new_x, new_y);
        QPixmap backgroundPixmap(GRAPHICS_FIELD_BACKGROUND_FILE_NAME);
        backgroundPixmap = backgroundPixmap.scaled(size, size, Qt::KeepAspectRatioByExpanding, Qt::SmoothTransformation);
        setPixmap(backgroundPixmap);

        setCells();
        setSignature();
        setShips();
    }

    template<class fieldType>
    void GraphicsField<fieldType>::refresh()
    {
        if (_refreshMode != FieldRefreshMode::notRefreshShips) {
            auto logicShips = _field->getShips();
            for (int i = 0; i < logicShips.size(); i++) {
                QString imageName = SHIP_IMAGE.at(logicShips[i].size());
                _ships[i]->setPixmap(QPixmap(imageName));
            }
            setShips();
        }

        foreachCell([this](QGraphicsPixmapItem *cell, int _x, int _y) {
                sea_battle::CellState state = _field->getCell(_x, _y);
                if (state != _cellStateField[_y][_x]) {
                    _cellStateField[_y][_x] = state;
                    QString imageName = CELL_STATE_IMAGES.at(state);
                    cell->setPixmap(QPixmap(imageName).scaled(_cellSize, _cellSize));
                }
        });
    }

    template<class fieldType>
    void GraphicsField<fieldType>::setShips()
    {
        auto &logicShips = _field->getShips();
        for (int i = 0; i < logicShips.size(); i++) {
            auto pixmap = _ships[i]->pixmap();
            pixmap = pixmap.scaledToHeight(_cellSize);
            _ships[i]->setPixmap(pixmap);

            auto [ship_x, ship_y] = convertCellCoordFromLogicFieldToPixel(logicShips[i].x(), logicShips[i].y());
            _ships[i]->setPos(ship_x, ship_y);

            if (logicShips[i].orientation() == sea_battle::Ship::Orientation::vertical) {
                _ships[i]->setTransformOriginPoint(_cellSize / 2, _cellSize / 2);
                _ships[i]->setRotation(90);
            }
        }

    }

    template<class fieldType>
    void GraphicsField<fieldType>::setCells()
    {
        foreachCell([this](QGraphicsPixmapItem *cell, int _x, int _y) {
            auto pixmap = cell->pixmap();
            pixmap = pixmap.scaled(_cellSize, _cellSize, Qt::IgnoreAspectRatio, Qt::FastTransformation);
            cell->setPixmap(pixmap);

            auto [pos_x, pos_y] = convertCellCoordFromLogicFieldToPixel(_x, _y);
            cell->setPos(pos_x, pos_y);

            _cellRectField[_y][_x]->setRect(pos_x, pos_y, _cellSize, _cellSize);
        });
    }

    template<class fieldType>
    void GraphicsField<fieldType>::setSignature()
    {
        qreal labelSize = _cellSize * _labelSizeRatio;
        qreal smallOffset = (_cellSize - labelSize) / 2;

        _font.setPointSize(labelSize * FONT_POINT_SIZE_RATIO);

        for (int i = 0; i < FIELD_CELLS_NUM; i++) {

            qreal offset = _cellSize * (i + 1);
            offset += smallOffset;

            _verticalSignature[i]->move(x() + smallOffset, y() + offset);
            _horizontalSignature[i]->move(x() + offset, y() + smallOffset);

            _verticalSignature[i]->resize(labelSize, labelSize);
            _horizontalSignature[i]->resize(labelSize, labelSize);

            _verticalSignature[i]->setFont(_font);
            _horizontalSignature[i]->setFont(_font);
        }
    }

    template<class fieldType>
    std::pair<int, int> GraphicsField<fieldType>::convertCellCoordFromLogicFieldToPixel(int _x, int _y)
    {
        return std::pair{_cellSize * (1 + _x), _cellSize * (1 + _y)};
    }

    template<class fieldType>
    void GraphicsField<fieldType>::foreachCell(std::function<void (QGraphicsPixmapItem *, int, int)> body)
    {
        for (int y = 0; y < FIELD_CELLS_NUM; y++) {
            for (int x = 0; x < FIELD_CELLS_NUM; x++){
                body(_cellPixmapField[y][x], x, y);
            }
        }
    }

}

#include "TritSetReference.h"


namespace tritset
{
	TritSetReference & TritSetReference::operator=(const Trit val) noexcept
	{
		data.setTrit(pos, val);
		return *this;
	}

	TritSetReference & TritSetReference::operator=(const TritSetReference & val) noexcept
	{
		data.setTrit(pos, Trit(val));
		return *this;
	}

	TritSetReference::operator Trit() const
	{
		return data.getTrit(pos);
	}

	TritSetReference::TritSetReference(TritSetData &data, size_t pos)
		:data(data), pos(pos)
	{
	}
}
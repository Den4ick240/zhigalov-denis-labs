package com.zhigalovLabs.Calculator;

public class Parser implements IParser {
    private static final String SEPARATOR = " ";

    @Override
    public String[] split(String inputLine) {
        return inputLine.split(SEPARATOR);
    }
}

#pragma once

#include <functional>
#include <vector>
#include <cstdint>

#include "Consts.h"

namespace sea_battle {

    class Ship {
    public:
        enum class Orientation {
            horizontal,
            vertical
        };

        explicit Ship(uint8_t size);

        void move(uint8_t x, uint8_t y);

        void rotate();

        bool doesCoverCell(int x, int y) const;

        bool doesFitIntoField() const;

        friend bool intersect(Ship &a, Ship &b);

        void foreachCell(std::function<void(int x, int y)> body);

        int size() const;
        int x() const;
        int y() const;
        Orientation orientation() const;

    protected:
        uint8_t _size;
        uint8_t _x, _y;
        Orientation _orientation;
    };

}

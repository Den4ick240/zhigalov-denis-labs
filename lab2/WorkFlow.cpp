#include "WorkFlow.h"
#include "worker.h"
#include <vector>
#include <unordered_map>
#include <string>
#include <exception>

namespace workflow {

    Executor::Executor(IBlockCreator *blockCreator)
            : blockCreator(blockCreator) {
    }

    void Executor::execute(IParser *parser, const std::string &inputFileName, const std::string &outputFileName) {
        std::unordered_map<int, IBlock *> blockMap;

        parser->ParseBlocks([this, &blockMap](const int &blockIndex, const std::string &blockName,
                                              const std::vector<std::string> &args) -> void {
            if (blockMap.find(blockIndex) != blockMap.end()) {
                throw exceptions::RepeatedId(blockIndex);
            }
            blockMap[blockIndex] = this->blockCreator->CreateBlock(blockIndex, blockName, args);
        });

        auto &chain = parser->getChainParser();

        try {
            bool inputFlag = false,
                    outputFlag = false;

            if (blockMap.at(*chain)->blockType != BlockType::input) {
                if (inputFileName.empty()) {
                    throw exceptions::NoIOFile(exceptions::NoIOFile::input);
                }
                inputFlag = true;
                Readfile(inputFileName).DoWork(text);
            }
            for (; !chain.is_finished(); ++chain) {
                auto block = blockMap.at(*chain);

                if (inputFlag && (block->blockType == BlockType::input)) {
                    throw exceptions::InvalidChainOrder();
                }
                inputFlag = inputFlag || (block->blockType == BlockType::input);

                if (outputFlag) {
                    throw exceptions::InvalidChainOrder();
                }
                outputFlag = outputFlag || (block->blockType == BlockType::output);

                block->DoWork(text);
            }
            if (blockMap.at(*chain)->blockType != BlockType::output) {
                if (outputFileName.empty()) {
                    throw exceptions::NoIOFile(exceptions::NoIOFile::output);
                }
                Writefile(outputFileName).DoWork(text);
            }
        }
        catch (std::out_of_range &e) {
            std::cout << e.what() << std::endl;
            throw exceptions::UndefinedId(*chain);
        }


        for (auto block : blockMap) {
            delete block.second;
        }
        blockMap.clear();
    }
}
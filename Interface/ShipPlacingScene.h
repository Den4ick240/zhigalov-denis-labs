#pragma once
#include "InterfaceConsts.h"
#include "ClickableField.h"
#include "GameScene.h"

namespace qt_interface {

class ShipPlacingScene : public GameScene {
        Q_OBJECT
    private:
        const qreal FIELD_SIZE_RATIO = 0.45;
        const qreal FIELD_OFFSET_RATIO = 0.025;
        const qreal BUTTON_SIZE_RATIO = 0.8;

    public:
        explicit ShipPlacingScene(const sea_battle::OwnField *field);

        void refreshButtons();

        sea_battle::Ship getShip() const;

    private:
        const sea_battle::OwnField *_field;
        ClickableField<sea_battle::OwnField> *_fieldItem;
        sea_battle::Ship _ship;
        std::map<int, QPushButton *> _resizeButtons;
        QGraphicsPixmapItem *_shipCursor;
        int _cellSize;

        void refreshShipRotation();

    public slots:
        void selectPlace(Qt::MouseButton button, int x, int y);

    private slots:
        void resizeButtonPressed();

        void mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;

        void mousePressEvent(QGraphicsSceneMouseEvent *event) override;

    signals:
        void placeSelected();
    };

}

package ru.nsu.ccfit.zhigalov;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;
import ru.nsu.ccfit.zhigalov.network.Connection;
import ru.nsu.ccfit.zhigalov.network.ConnectionObserver;
import ru.nsu.ccfit.zhigalov.network.TextMessage;
import ru.nsu.ccfit.zhigalov.network.messages.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledThreadPoolExecutor;

public class MainWindowController implements ConnectionObserver {

    private static final Font MESSAGE_FONT = new Font(15);
    private static final Integer THREAD_POOL_CORE_SIZE = 1;
    private static final Integer WRAPPING_WIDTH = 360;
    private static final Integer USER_ONLINE_WRAPPING_WIDTH = 114;
    private static final String serverHost = "127.0.0.1";
    private static final Integer serverPort = 24000;
    private static final String CONNECTION_CLOSED = "Connection closed";
    private Connection connection;
    private static ScheduledThreadPoolExecutor threadPool;
    private int sessionId;
    private String nickname;
    private Integer usersOnlineNum = 0;
    private Map<String, Text> usersOnline = new HashMap<>();

    @FXML
    public void initialize() {
        threadPool = (ScheduledThreadPoolExecutor)Executors.newScheduledThreadPool(THREAD_POOL_CORE_SIZE);
        Connection.setThreadPool(threadPool);
        try {
            connection = new Connection(this, serverHost, serverPort);
        } catch (IOException e) {
            e.printStackTrace();
            warningLabel.setText(e.getMessage());
        }
        messagePane.vvalueProperty().bind(container.heightProperty());
//        messagePane.setVvalue(1D);
    }

    public void shutdown() {
        System.out.println("shutdown");
        if (connection.isConnected()) {
            System.out.println("disconnect");
            connection.sendObject(new Disconnect());
            connection.disconnect();
        }
        threadPool.shutdown();
        Platform.exit();

    }



    @FXML
    private ScrollPane usersOnlinePane;

    @FXML
    private ScrollPane messagePane;

    @FXML
    private TextFlow usersOnlineTextFlow;

    @FXML
    private Button sendButton;

    @FXML
    private TextArea messageTextField;

    @FXML
    private Button exitButton;

    @FXML
    private Label usersNumLabel;

    @FXML
    private TextField newNickArea;

    @FXML
    private Button nicknameButton;

    @FXML
    private Label warningLabel;

    @FXML
    private VBox usersOnlineContainer;

    @FXML
    private ColorPicker colorPicker;

    @FXML
    private VBox container;

    @FXML
    void changeNicknameButton(ActionEvent event) {
        String newNick = newNickArea.getText();
        if (newNick.equals(nickname)) {
            return;
        }
        connection.sendObject(new ChangeNicknameRequest(newNick));
    }

    @FXML
    void exitButtonAction(ActionEvent event) {
        Platform.exit();
    }

    @FXML
    void sendButtonAction(ActionEvent event) {
        var msg = new TextMessage(nickname, messageTextField.getText(), sessionId);
        messageTextField.clear();
        connection.sendObject(new NewTextMessage(msg));
    }

    @Override
    public synchronized void onConnectionReady(Connection connection) {

    }

    @Override
    public synchronized void onReceiveObject(Connection connection, IMessage msg) {
        Class msgClass = msg.getClass();
        System.out.println(msg.getClass());
        Platform.runLater(new Runnable() {
            @Override
            public void run() {

                if (msgClass.equals(ServerInfo.class)) {
                    sessionId = ((ServerInfo) msg).getId();
                    nickname = ((ServerInfo) msg).getNickname();
                    newNickArea.setText(nickname);
                    UpdateMessageSpace(((ServerInfo) msg).getMessages());
                    UpdateUsersOnline(((ServerInfo) msg).getUsersOnline());
                    return;
                }

                if (msgClass.equals(NewTextMessage.class)) {
                    addMessage(((NewTextMessage) msg).getMessage());
                    return;
                }

                if (msgClass.equals(ClientConnected.class)) {
                    userConnected(((ClientConnected) msg).getName());
                    return;
                }

                if (msgClass.equals(ClientDisconnected.class)) {
                    userDisconnected(((ClientDisconnected) msg).getName());
                    return;
                }

                if (msgClass.equals(ChangeNicknameNotify.class)) {
                    String oldNick = ((ChangeNicknameNotify) msg).getOldNick();
                    String newNick = ((ChangeNicknameNotify) msg).getNewNick();
                    var text = usersOnline.get(oldNick);
                    usersOnline.remove(oldNick);
                    text.setText(newNick);
                    usersOnline.put(newNick, text);
                    return;
                }

                if (msgClass.equals(ChangeNicknameResponse.class)) {
                    if (!((ChangeNicknameResponse) msg).isChanged()) {
                        warningLabel.setText(((ChangeNicknameResponse) msg).getReason());
                    }
                    else {
                        warningLabel.setText("");
                    }
                    nickname = ((ChangeNicknameResponse) msg).getNickname();
                    newNickArea.setText(nickname);
                }
            }
        });


    }

    private void addMessage(TextMessage message) {
        var text = getTextFromMessage(message);
        text.setFont(MESSAGE_FONT);
        container.getChildren().add(text);
    }

    private void userConnected(String name) {
        var text = new Text(name);
        usersOnline.put(name, text);
        text.setWrappingWidth(USER_ONLINE_WRAPPING_WIDTH);
        usersOnlineContainer.getChildren().add(text);
        usersOnlineNum++;
        usersNumLabel.setText(usersOnlineNum.toString());
    }

    private void userDisconnected(String name) {
        usersOnlineContainer.getChildren().remove(usersOnline.get(name));
        usersOnline.remove(name);
        usersOnlineNum--;
        usersNumLabel.setText(usersOnlineNum.toString());
    }

    private void UpdateUsersOnline(ArrayList<String> usersOnline) {
        for (var user : usersOnline) {
            userConnected(user);
        }
    }

    private Text getTextFromMessage(TextMessage msg) {
        var out = new Text(msg.getSenderName() + ":\n" + msg.getMessage());
            out.setWrappingWidth(WRAPPING_WIDTH);
        if (sessionId == msg.getSenderId()) {
            out.setTextAlignment(TextAlignment.RIGHT);
        }
        return out;
    }

    private void UpdateMessageSpace(ArrayList<TextMessage> messages) {
        for (var message : messages) { addMessage(message); }
    }

    @Override
    public synchronized void onDisconnect(Connection connection) {
        warningLabel.setText(CONNECTION_CLOSED);
    }

    @Override
    public synchronized void onException(Connection connection, Exception e) {
        e.printStackTrace();
    }
}

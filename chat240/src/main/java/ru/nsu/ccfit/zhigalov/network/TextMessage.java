package ru.nsu.ccfit.zhigalov.network;


import java.io.Serializable;

public class TextMessage implements Serializable {
    String senderName;

    public int getSenderId() {
        return senderId;
    }

    int senderId;
    String message;


    public String getSenderName() {
        return senderName;
    }

    public String getMessage() {
        return message;
    }

    public TextMessage(String senderName, String message, int senderId) {
        this.senderName = senderName;
        this.message = message;
        this.senderId = senderId;
    }


}

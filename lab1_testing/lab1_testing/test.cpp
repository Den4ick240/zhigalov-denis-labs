#include "../../lab1/lab1/tritset.h"
#include "gtest/gtest.h"

using tritset::True;
using tritset::False;
using tritset::Unknown;
using tritset::Trit;
using tritset::TritSet;

namespace
{
	class RandomTritSet
	{
	public:
		const size_t TRIT_NUM;
		Trit *arr;
		TritSet set;

		RandomTritSet(size_t trit_num = 10000)
			: TRIT_NUM(trit_num), set(trit_num)
		{
			Trit values[3] = { False, Unknown, True };
			arr = new Trit[TRIT_NUM];
			for (size_t i = 0; i < TRIT_NUM; i++)
			{
				arr[i] = values[rand() % 3];
				set[i] = arr[i];
			}
		}

		~RandomTritSet()
		{
			delete[] arr;
		}
	};
}

namespace ConstructorsAndAssigningTest
{
	TEST(ConstructorsAndAssigningTest, UsualConstuctorTest_NoParams)
	{
		TritSet set;
		ASSERT_EQ(set.size(), 16);
		EXPECT_EQ(set.capacity(), 1);
		for (int i = 0; i < 16; i++)
		{
			ASSERT_EQ(Trit(set[i]), Unknown);
		}
	}
	TEST(ConstructorsAndAssigningTest, UsualConstructorTest_WithParam)
	{
		TritSet set(70, False);
		ASSERT_EQ(set.size(), 70);
		EXPECT_EQ(set.capacity(), 5);
		for (int i = 0; i < 70; i++)
		{
			ASSERT_EQ(Trit(set[i]), False);
		}
	}
	TEST(ConstructorsAndAssigningTest, AssigningOperatorTest)
	{
		RandomTritSet randSet;
		
		randSet.set[2] = True;
		if (true)
		{
			TritSet set2;
			set2 = randSet.set;
			for (int i = 0; i < randSet.TRIT_NUM; i++)
			{
				ASSERT_EQ(Trit(set2[i]), randSet.set[i]);
			}
		}
		ASSERT_EQ(Trit(randSet.set[2]), True);
	}
}

namespace SizeChangingTest
{
	TEST(SizeChangingTest, GetLengthTest)
	{
		TritSet set(40, Unknown);
		ASSERT_EQ(0, set.length());

		set[5] = True;
		ASSERT_EQ(6, set.length());

		set[30] = False;
		ASSERT_EQ(31, set.length());
	}

	TEST(SizeChangingTest, ShrinkTest)
	{
		TritSet set(20);

		set[30] = True;
		set[30] = Unknown;

		set[25] = True;

		set.shrink();
		ASSERT_EQ(set.size(), 26);

		set[25] = Unknown;
		set.shrink();
		ASSERT_EQ(set.size(), 20);
	}

	TEST(SizeChangingTest, TrimTest)
	{
		TritSet set(20);

		set.trim(30);
		ASSERT_EQ(set.size(), 20);

		set.trim(10);
		ASSERT_EQ(set.size(), 10);
		ASSERT_EQ(set.capacity(), 1);
	}

	TEST(SizeChangingTest, CardinalityTest)
	{
		RandomTritSet randomTritSet;
		std::unordered_map<Trit, int> test_map;

		for (size_t i = randomTritSet.TRIT_NUM; i > randomTritSet.TRIT_NUM / 2; i--)
		{
			randomTritSet.set[i] = Unknown;
		}
		for (size_t i = 0; i < randomTritSet.set.length(); i++)
		{
			test_map[randomTritSet.arr[i]]++;
		}

		std::unordered_map<Trit, int> res_map = randomTritSet.set.cardinality();
		ASSERT_EQ(test_map[True], res_map[True]);
		ASSERT_EQ(test_map[False], res_map[False]);
		ASSERT_EQ(test_map[Unknown], res_map[Unknown]);
	}

}

namespace AccessingToTritsTest
{
	TEST(AccessingToTritsTest, ReferenceGetTest)
	{
		RandomTritSet randomTritSet;

		for (size_t i = 0; i < randomTritSet.TRIT_NUM; i -= -1)
		{
			ASSERT_EQ(Trit(randomTritSet.set[i]), randomTritSet.arr[i]);
		}
	}

	TEST(AccessingToTritTest, ReferenceSetTest)
	{
		TritSet set(10, False);

		set[4] = True;
		ASSERT_EQ(Trit(set[4]), True);

		set[5] = set[4];
		ASSERT_EQ(Trit(set[5]), True);

		ASSERT_TRUE(set[5] == set[4]);
	}
}

namespace TritSetOperatorsTest
{
	void OperatorTesting(TritSet(*TritSetOperation)(const TritSet&, const TritSet&), Trit(*operation)(const Trit, const Trit))
	{
		RandomTritSet right(20000), left(10000);
		TritSet res(TritSetOperation(right.set, left.set));
		for (size_t i = 0; i < 10000; i++)
		{
			ASSERT_EQ(Trit(res[i]), operation(right.arr[i], left.arr[i]));
		}
		for (size_t i = 10000; i < 20000; i++)
		{
			ASSERT_EQ(Trit(res[i]), operation(right.arr[i], Unknown));
		}
	}

	TEST(TritSetOperatorsTest, AndOperatorTest) {
		OperatorTesting(tritset::operator&, tritset::operator&);
	}
	TEST(TritSetOperatorsTest, OrOperatorTest)	{
		OperatorTesting(tritset::operator|, tritset::operator|);
	}
	TEST(TritSetOperatorsTest, XorOperatorTest)	{
		OperatorTesting(tritset::operator^, tritset::operator^);
	}
	TEST(TritSetOperatorsTest, NotOperatorTest)	{
		OperatorTesting(
			[](const TritSet& set, const TritSet& unused) { return ~set; },
			[](const Trit trit, const Trit unused) { return ~trit; }
		);
	}

	TEST(TritSetOperatorsTest, EqualOperatorTest) {
		TritSet a(20), b(30);
		ASSERT_TRUE(a == b);

		for (int i = 0; i < 10; i++)
		{
			a[i] = b[i] = True;
		}
		ASSERT_TRUE(a == b);

		b[25] = False;
		ASSERT_TRUE(a != b);

		a[25] = False;
		ASSERT_TRUE(a == b);

		a[0] = False;
		ASSERT_TRUE(a != b);
	}
}

namespace TritOperatorsTest
{
	TEST(TritOperatorsTest, AndOperatorTest)
	{
		EXPECT_EQ(True & True, True);
		EXPECT_EQ(False & False, False);
		EXPECT_EQ(Unknown & Unknown, Unknown);
		EXPECT_EQ(True & False, False);
		EXPECT_EQ(True & Unknown, Unknown);
		EXPECT_EQ(False & Unknown, False);
	}

	TEST(TritOperatorsTest, OrOperatorTest)
	{
		EXPECT_EQ(True | True, True);
		EXPECT_EQ(False | False, False);
		EXPECT_EQ(Unknown | Unknown, Unknown);
		EXPECT_EQ(True | False, True);
		EXPECT_EQ(True | Unknown, True);
		EXPECT_EQ(False | Unknown, Unknown);
	}

	TEST(TritOperatorsTest, XorOperatorTest)
	{
		EXPECT_EQ(True ^ True, False);
		EXPECT_EQ(False ^ False, False);
		EXPECT_EQ(Unknown ^ Unknown, Unknown);
		EXPECT_EQ(True ^ False, True);
		EXPECT_EQ(True ^ Unknown, Unknown);
		EXPECT_EQ(False ^ Unknown, Unknown);
	}

	TEST(TritOperatorsTest, NotOperatorTest)
	{
		EXPECT_EQ(~True, False);
		EXPECT_EQ(~False, True);
		EXPECT_EQ(~Unknown, Unknown);
	}
}
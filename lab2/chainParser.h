#pragma once

#include "WorkFlow_general.h"
#include "exceptions.h"
#include "parserInterface.h"
#include <vector>
#include <iostream>

namespace workflow {

    class ChainParser : public IChainParser {
    public:
        explicit ChainParser(std::istream &inputStream);

        IChainParser &operator++() override;

        int operator*() override;

        bool is_finished() override;

    private:
        std::istream &inputStream;
        bool finished = false;
        int val;

        void parseIndex();

        void parseNext();

        friend class Parser;
    };
}
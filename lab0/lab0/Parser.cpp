#include "Parser.h"
#include <string>
#include <map>

namespace Zhigalov
{
	using std::map;
	using std::string;	

	Parser::Parser(map<string, int> & word_map)
		:word_map(word_map)
	{
	}

	void Parser::parse_string(const string & str)
	{
		string buff;
		for (int i = 0; i < str.size(); i++)
		{
			if (!is_separator(str[i]))
			{
				buff += str[i];
			}
			else if (!buff.empty())
			{
				word_map[buff]++;
				buff.clear();
			}
		}
		if (!buff.empty())
		{
			word_map[buff]++;
		}
	}

	bool Parser::is_separator(char a)
	{
		if (a >= 'A' && a <= 'Z')
		{
			return false;
		}
		if (a >= 'a' && a <= 'z')
		{
			return false;
		}
		if (a >= '0' && a <= '9')
		{
			return false;
		}
		return true;
	}
}
#include "RandomBotPlayer.h"

#include <ctime>

namespace sea_battle {

    void RandomBotPlayer::placeShips() {
        auto &unplacedShips = _ownField.getUnplacedShips();

        while (!unplacedShips.empty()) {
            int size = rand() % unplacedShips.size();
            auto iter = unplacedShips.cbegin();
            while (size--) {
                iter++;
            }
            size = iter->first;
            Ship ship(size);
            ship.move(rand() % FIELD_WIDTH, rand() % FIELD_HEIGHT);
            if (rand() % 2 == 0) {
                ship.rotate();
            }
            _ownField.tryToAddShip(ship);
        }
    }

    RandomBotPlayer::RandomBotPlayer(OwnField &ownField, EnemyField &enemyField)
            : IPlayer(ownField, enemyField), rand(time(NULL))/*, _freeCellsAmount(FIELD_WIDTH * FIELD_HEIGHT)*/ {
        for (int y = 0; y < FIELD_HEIGHT; y++) {
            for (int x = 0; x < FIELD_WIDTH; x++) {
                _freeCells.push_back({x, y});
            }
        }
    }

    bool RandomBotPlayer::move() {
        int cellNumber = rand() % _freeCells.size();
        auto[x, y] = _freeCells[cellNumber];
        _freeCells[cellNumber] = _freeCells.back();
        _freeCells.pop_back();

        return _enemyField.shot(x, y);
    }

}

package ru.nsu.ccfit.zhigalov.network.messages;

import ru.nsu.ccfit.zhigalov.network.messages.IMessage;

public class ClientDisconnected extends IMessage {
    public ClientDisconnected(String name) {
        this.name = name;
    }

    String name;

    public String getName() {
        return name;
    }
}

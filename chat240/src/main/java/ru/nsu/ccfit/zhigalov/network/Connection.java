package ru.nsu.ccfit.zhigalov.network;

import ru.nsu.ccfit.zhigalov.network.messages.IMessage;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class Connection {

    private final Socket socket;
    private final InputStream socketInputStream;
    private final ObjectOutputStream out;
    private final ObjectInputStream in;
    private Runnable task;
    private ScheduledFuture<?> scheduledFuture;
    private static ScheduledThreadPoolExecutor threadPool;
    private ConnectionObserver observer;
    private static final Integer SCHEDULE_INITIAL_DELAY = 0;
    private static final Integer SCHEDULE_PERIOD = 100;
    private static final TimeUnit SCHEDULE_TIMEUNIT = TimeUnit.MILLISECONDS;
    private final int id;
    private static int nextId = 0;

    public Integer getId() {
        return id;
    }

    public static void setThreadPool(ScheduledThreadPoolExecutor _threadPool) {
        threadPool = _threadPool;
    }

    public boolean isConnected() {
        return socket.isConnected();
    }

    public Connection(ConnectionObserver observer, String host, int port) throws IOException {
        this(observer, new Socket(host, port));
    }

    public Connection(ConnectionObserver observer_, Socket socket) throws IOException {
        threadPool.setRemoveOnCancelPolicy(true);
        this.socket = socket;
        socketInputStream = socket.getInputStream();
        out = new ObjectOutputStream(socket.getOutputStream());
        in = new ObjectInputStream(socketInputStream);
        this.observer = observer_;
        id = nextId++;
        observer.onConnectionReady(this);

        task = new Runnable() {
            public void run() {
                try {
                    if (socketInputStream.available() < 1) {
                        return;
                    }
                    IMessage msg = (IMessage) in.readObject();
                    observer.onReceiveObject(Connection.this, msg);
                } catch (Exception e) {
                    observer.onException(Connection.this, e);
                }
            }
        };
        scheduledFuture = threadPool.scheduleAtFixedRate(task, SCHEDULE_INITIAL_DELAY, SCHEDULE_PERIOD, SCHEDULE_TIMEUNIT);
    }

    public void sendObject(IMessage msg) {
        try {
            out.writeObject(msg);
            out.flush();
        } catch (IOException e) {
            observer.onException(this, e);
            disconnect();
        }
    }

    public synchronized void disconnect() {
        scheduledFuture.cancel(true);
        try {
            socket.close();
        } catch (IOException e) {
            observer.onException(this, e);
        }
    }

    @Override
    public String toString() {
        return "TCP connection " + socket.getInetAddress() + ": " + socket.getPort() + " connectionId: " + getId().toString();
    }
}

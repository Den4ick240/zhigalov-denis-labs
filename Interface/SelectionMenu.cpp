#include "SelectionMenu.h"

namespace qt_interface {

    SelectionMenu::SelectionMenu(const std::set<std::string> &optionSet, const std::string &labelText)
        : _optionSet(optionSet)
    {
        int itemWidth = VIEW_WIDTH * WIDTH_RATIO;
        int offset = VIEW_HEIGH / (optionSet.size() + 1);
        int itemHeigh = offset * HEIGH_RATIO;
        int currentOffset = (offset - itemHeigh) / 2;
        int horizontalOffset = (VIEW_WIDTH - itemWidth) / 2;

        QFont font = STANDART_FONT;
        font.setPointSize(itemHeigh * FONT_POINT_SIZE_RATIO);

        _label = new QLabel();
        _label->setText(("Chose " +labelText).c_str());
        _label->move(horizontalOffset, currentOffset);
        _label->resize(itemWidth, itemHeigh);
        _label->setFont(font);
        _label->setAlignment(Qt::AlignCenter);
        _label->setStyleSheet(ITEM_STYLE_SHEET);
        addWidget(_label);

        for (auto type : optionSet) {
            auto *button = new QPushButton(type.c_str());
            addWidget(button);

            _buttonVector.push_back(button);
            button->resize(itemWidth, itemHeigh);
            button->setFont(font);
            button->setStyleSheet(ITEM_STYLE_SHEET);

            currentOffset += offset;
            button->move(horizontalOffset, currentOffset);
            connect(button, SIGNAL(clicked()), this, SLOT(buttonPressed()));
        }
    }

    SelectionMenu::~SelectionMenu()
    {
        delete _label;
        for (auto *item : _buttonVector) {
            delete item;
        }
    }

    std::string &SelectionMenu::getSelectedOption()
    {
        return _selectedOption;
    }

    void SelectionMenu::buttonPressed()
    {
        auto findButton = std::find(_buttonVector.begin(), _buttonVector.end(), sender());
        if (findButton == _buttonVector.end()) {
            throw std::exception();
        }

        auto *button = *findButton;
        _selectedOption = button->text().toStdString();
        emit optionSelected();
    }

}

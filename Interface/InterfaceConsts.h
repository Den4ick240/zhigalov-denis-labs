#pragma once

#include <QGraphicsScene>
#include <QPushButton>
#include <QGraphicsView>
#include <QLabel>
#include <QDebug>
#include <QFont>
#include <QString>
#include <QBrush>
#include <QImage>
#include <QColor>
#include <QEventLoop>
#include <QGraphicsRectItem>
#include <QGraphicsPixmapItem>
#include <QGraphicsSceneMouseEvent>
#include <QTimer>
#include <QApplication>

#include <type_traits>
#include <string>
#include <functional>
#include <set>

#include "../lab3/Field.h"

namespace qt_interface {
    const int VIEW_WIDTH = 1900;
    const int VIEW_HEIGH = 980;
    const QColor BACKGROUND_COLOR(0, 77, 99);
    const QColor ITEM_COLOR(0, 139, 220);
    const QColor TEXT_COLOR(0, 30, 38);
    const QString ITEM_STYLE_SHEET = "background-color : rgb(0, 139, 220); color : rgb(0, 30, 38)";
    const QFont STANDART_FONT("Georgia", 1, QFont::StyleItalic);
    const qreal FONT_POINT_SIZE_RATIO = 0.35;
    const QBrush BACKGROUND_BRUSH = QBrush(BACKGROUND_COLOR);

    const QString FIELD_BUTTON_STYLE_SHEET = "QPushButton:hover{ background-color : rgb(200, 0, 0, 50);} QPushButton{ background : transparent;}";
    const QString GRAPHICS_FIELD_BACKGROUND_FILE_NAME(":/images/OceanBackground.jpg");
    const int FIELD_CELLS_NUM = 10;
    const QString FIELD_HORIZONTAL_SIGNATURE[FIELD_CELLS_NUM] = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J"};
    const QString FIELD_VERTICAL_SIGNATURE[FIELD_CELLS_NUM] = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};

    const std::map<int, QString> SHIP_IMAGE = {
        {1, ":/images/1ship.gif"},
        {2, ":/images/2ship.gif"},
        {3, ":/images/3ship.gif"},
        {4, ":/images/4ship.gif"}
    };
    const std::map<sea_battle::CellState, QString> CELL_STATE_IMAGES = {
        {sea_battle::CellState::hit, ":/images/HitState.gif"},
        {sea_battle::CellState::dead, ":/images/DeadState.gif"},
        {sea_battle::CellState::clear, ":/images/ClearState.gif"},
        {sea_battle::CellState::miss, ":/images/ExludedState.gif"},
        {sea_battle::CellState::alive, ":/images/ClearState.gif"},
        {sea_battle::CellState::excluded, ":/images/ExludedState.gif"}
    };

}

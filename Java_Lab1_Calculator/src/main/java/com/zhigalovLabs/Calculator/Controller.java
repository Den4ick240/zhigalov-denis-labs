package com.zhigalovLabs.Calculator;

import com.zhigalovLabs.Calculator.factory.CommandFactory;
import com.zhigalovLabs.Calculator.commands.ICommand;
import com.zhigalovLabs.Calculator.exceptions.CalculatorException;
import com.zhigalovLabs.Calculator.exceptions.ConfigFileException;
import com.zhigalovLabs.Calculator.exceptions.EmptyStack;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.EmptyStackException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Controller {
    private final BufferedReader bufferedReader;
    private final CommandFactory commandFactory;
    private final IParser parser;
    private CommandContext commandContext;
    private final Logger logger = Main.getLogger();

    public Controller(InputStreamReader reader) throws ConfigFileException {
        bufferedReader = new BufferedReader(reader);
        commandFactory = new CommandFactory();
        parser = new Parser();
        commandContext = new CommandContext();
    }

    public void run() throws IOException {
        while (true) {
            try {
                String buffer = bufferedReader.readLine();
                executeLine(buffer);
            } catch (CalculatorException e) {
                System.out.println(e.getMessage());
                logger.log(Level.WARNING, e.getMessage(), e);
            } catch (EmptyStackException e) {
                System.out.println((new EmptyStack()).getMessage());
                logger.log(Level.WARNING, "Empty stack", e);
            }

        }
    }

    public void executeLine(String buffer) throws CalculatorException {
        String[] arguments;
        ICommand command;
        if (buffer.trim().isEmpty()) {
            logger.fine("Empty line");
            return;
        }
        logger.fine(buffer);
        arguments = parser.split(buffer);
        command = commandFactory.createCommand(arguments);
        command.run(commandContext);

    }

}

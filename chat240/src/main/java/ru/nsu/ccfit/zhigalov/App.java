package ru.nsu.ccfit.zhigalov;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * JavaFX App
 */
public class App extends Application {

    private static Scene scene;
    private static MainWindowController controller;
    public static final String FXML_FILE_NAME = "mainwindow.fxml";

    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(FXML_FILE_NAME));
        scene = new Scene(fxmlLoader.load());
        controller = fxmlLoader.getController();
        System.out.println(controller == null);
        stage.setScene(scene);
        stage.setOnHidden(e -> {
            controller.shutdown();
        });
        stage.setResizable(false);
        stage.show();
    }

    @Override
    public void stop() {
    }

    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch();
    }

}
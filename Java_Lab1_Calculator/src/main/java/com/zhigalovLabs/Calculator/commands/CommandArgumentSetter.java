package com.zhigalovLabs.Calculator.commands;

import com.zhigalovLabs.Calculator.exceptions.NotEnoughCommandArguments;

public class CommandArgumentSetter  {
    private String[] args;
    private final int expectedArgc;
    private static final String UNUSED_ARGUMENTS_WARNING = "Warning: %d of %d arguments are unused for command %s\n";
    public void setArguments(String[] args) throws NotEnoughCommandArguments {
        int argc = args.length - 1;
        if (expectedArgc < argc) {
            System.out.print(argc - expectedArgc);
            System.out.format(UNUSED_ARGUMENTS_WARNING, argc - expectedArgc, argc, args[0]);
        }
        if (expectedArgc > argc) {
            throw new NotEnoughCommandArguments();
        }
        this.args = args;
    }

    protected CommandArgumentSetter(int argc) {
        expectedArgc = argc;
    }

    protected String[] getArgs() {
        return args;
    }
}

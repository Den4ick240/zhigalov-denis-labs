#pragma once

#include "../lab3/IGameInterface.h"

#include "InterfaceConsts.h"
#include "SelectionMenu.h"
#include "ShipPlacingScene.h"
#include "BattleScene.h"

namespace qt_interface {

    class GameInterface : public QObject, public sea_battle::IGameInterface{
        Q_OBJECT

    public:
        GameInterface(QGraphicsView *view);

        GameInterface() = default;

        std::string choosePlayerType(const std::set<std::string> &playerTypesSet, const std::string &playerName) override;

        sea_battle::Ship getNextShipToPlace(const std::map<int, int> &unplacedShips) override;

        MoveStruct getNextMove(const sea_battle::OwnField &ownField, const sea_battle::EnemyField &enemyField) override;

        void setView(QGraphicsView *view);

        void setPlacingScene(ShipPlacingScene *);

        void setBattleScene(BattleScene *);

    private:
        QGraphicsView *_view;
        ShipPlacingScene *_shipPlacingScene;
        BattleScene *_battleScene;
        QEventLoop *_loop;

        void waitFor(QObject *sender, const char *signal);
    };
}

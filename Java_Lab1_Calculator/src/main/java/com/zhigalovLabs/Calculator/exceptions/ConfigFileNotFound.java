package com.zhigalovLabs.Calculator.exceptions;

public class ConfigFileNotFound extends ConfigFileException {
    @Override
    public String getMessage() {
        return "Couldn't open command config file";
    }
}

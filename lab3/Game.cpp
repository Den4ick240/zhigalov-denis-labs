#include "Game.h"

namespace sea_battle {

    const std::vector<std::string> Game::PlayerTypeNames = {
            "Manual player",
            "Random bot",
            "Smart bot"
    };
    const std::set<std::string> Game::POSSIBLE_FIRST_PLAYERS = {
            Game::PlayerTypeNames[0],
            Game::PlayerTypeNames[1],
            Game::PlayerTypeNames[2],
    };

    const std::set<std::string> Game::POSSIBLE_SECOND_PLAYERS = {
            Game::PlayerTypeNames[1],
            Game::PlayerTypeNames[2],
    };

    Game::Game(IGameInterface *interface)
            : _interface(interface), _players{nullptr}, _fields{nullptr} {}

    void Game::createPlayers() {
        std::set<std::string> playerTypesSet;
        std::string chosenPlayerType;

        for (OwnField *&field : _fields) {
            field = new OwnField();
        }

        chosenPlayerType = _interface->choosePlayerType(POSSIBLE_FIRST_PLAYERS, _playerNames[0]);
        _players[0] = playerTypeAndCreatorMap.at(chosenPlayerType)(*_fields[0], *_fields[1]);

        chosenPlayerType = _interface->choosePlayerType(POSSIBLE_SECOND_PLAYERS, _playerNames[1]);
        _players[1] = playerTypeAndCreatorMap.at(chosenPlayerType)(*_fields[1], *_fields[0]);
    }

    void Game::arrangeShips() {
        _players[0]->placeShips();
        _players[1]->placeShips();
    }

    bool Game::gameFrame() {
        static int offender = 0, defender = 1;

        if (_fields[defender]->allShipsDead()) {
            return false;
        }

        if (!_players[offender]->move()) {
            offender = (offender + 1) % 2;
            defender = (defender + 1) % 2;
        }
        return true;
    }

    void Game::endGame() {
        for (int i = 0; i < PLAYERS_NUM; i++) {
            delete _players[i];
            delete _fields[i];
            _players[i] = nullptr;
            _fields[i] = nullptr;
        }
    }

    Game::~Game() {
    }
}

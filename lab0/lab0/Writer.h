#pragma once
#include <fstream>
#include <vector>
#include <cstdlib>
#include <map>
#include <string>
#include <algorithm>

namespace Zhigalov
{
	class Writer
	{
	public:
		virtual ~Writer();
		void open_file(const std::string & file_name);
		void count_words(const std::map<std::string, int> & word_map); //makes a sorted vector of words and frequences from map
		void write_words();

	private:
		std::ofstream out_file;
		std::vector<std::pair<std::string, int>> word_vector;
		int word_cnt;
	};

}
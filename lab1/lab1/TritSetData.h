#pragma once
#include "TritSetGeneral.h"

namespace tritset
{
	class TritSetData
	{
	private:
		unit * arr;
		size_t arr_size;
		size_t trit_num;
		
		TritSetData(size_t size, Trit val);
		virtual ~TritSetData();
	
		Trit getTrit(size_t i) const;
		Trit setTrit(size_t i, Trit val);
		void resize(size_t new_size);				//changes size of allocated memory if necessary

		static int trit_pos(int i);					//calculates trit position in the unit by trit index
		static size_t unit_pos(int i);				//calculates index of the unit which contains trit with index i
													// ^^^ also calculates how many units you have to allocate in order to store (i + 1) trits 
		static unit get_unknown_mask(unit a);		//generates a mask where in places of unknown trits are 00 and in other places are 11
	
		TritSetData& operator&=(const TritSetData& obj);
		TritSetData& operator|=(const TritSetData& obj);
		TritSetData& operator^=(const TritSetData& obj);
		TritSetData& flip();

		friend class TritSet;
		friend class TritSetReference;
		friend bool operator==(const TritSet & left, const TritSet & right);
	};

}
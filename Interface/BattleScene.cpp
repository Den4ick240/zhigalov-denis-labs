#include "BattleScene.h"

namespace qt_interface {

    BattleScene::BattleScene(const sea_battle::OwnField *ownField, const sea_battle::EnemyField *enemyField)
        : _ownField(ownField), _enemyField(enemyField) {

        int halfViewSize = VIEW_WIDTH / 2;
        int fieldSize = halfViewSize * FIELD_SIZE_RATIO;
        int fieldOffset = halfViewSize - fieldSize;
        fieldOffset /= 2;

        _graphicsOwnField = new GraphicsOwnFieldType(_ownField, fieldSize, fieldOffset, fieldOffset, FieldRefreshMode::notRefreshShips);
        _graphicsEnemyField = new GraphicsEnemyFieldType(_enemyField, fieldSize, halfViewSize + fieldOffset, fieldOffset);

        _graphicsOwnField->addToScene(this);
        _graphicsEnemyField->addToScene(this);

        _graphicsEnemyField->connectButtonsToSlot(this, SLOT(moveHandler(Qt::MouseButton, int, int)));

        _winnerLabel = new QLabel();
        _winnerLabel->resize(fieldSize, fieldOffset);
        _winnerLabel->move(fieldOffset, 0);
        _winnerLabel->setAlignment(Qt::AlignCenter);
        QFont font = STANDART_FONT;
        font.setPointSize(_winnerLabel->height() * FONT_POINT_SIZE_RATIO);
        _winnerLabel->setFont(font);
        _winnerLabel->setStyleSheet("background: transparent");
        addWidget(_winnerLabel);

        _exitButton = new QPushButton();
        _exitButton->setText("Exit");
        _exitButton->setStyleSheet(ITEM_STYLE_SHEET);

        int button_heignt = VIEW_HEIGH * EXIT_BTN_HEIGHT_RATIO;
        int button_width = button_heignt * EXIT_BTN_WITHS_TO_HEIGHT_RATIO;
        int button_offset = button_heignt * EXIT_BTN_OFFSET_RATIO;
        int button_x = VIEW_WIDTH - button_width - button_offset;
        int button_y = VIEW_HEIGH - button_heignt - button_offset;

        _exitButton->resize(button_width, button_heignt);
        _exitButton->move(button_x, button_y);
        _exitButton->setFont(font);
        this->addWidget(_exitButton);
    }

    sea_battle::IGameInterface::MoveStruct BattleScene::getMove()
    {
        return _lastMove;
    }

    void BattleScene::refresh()
    {
        _graphicsOwnField->refresh();
        _graphicsEnemyField->refresh();
    }

    void BattleScene::endGame(const std::string &winnerName, int winnerId)
    {
        this->addWidget(_winnerLabel);
        _winnerLabel->setText((winnerName + " is the winner!").c_str());
        _winnerLabel->setStyleSheet(ITEM_STYLE_SHEET);

        if (winnerId != 0) {
            _winnerLabel->move(_winnerLabel->x() + VIEW_WIDTH / 2, _winnerLabel->y());
        }
    }

    void BattleScene::connectToExitButton(QObject *obj, const char *slot)
    {
        connect(_exitButton, SIGNAL(clicked()), obj, slot);
    }

    void BattleScene::moveHandler(Qt::MouseButton button, int x, int y)
    {
        _lastMove.x = x;
        _lastMove.y = y;
        if (button == Qt::RightButton) {
            _lastMove.moveType = sea_battle::IGameInterface::MoveStruct::exclude;
        }
        else {
            _lastMove.moveType = sea_battle::IGameInterface::MoveStruct::shoot;
        }
        emit moveDoneSignal();
    }

}

#pragma once
#include <unordered_map>
#include <iostream>
#include <map>
#include "TritSetGeneral.h"
#include "TritSetData.h"
#include "TritSetReference.h"

namespace tritset
{
	class TritSet
	{
	public:
		TritSet(size_t size = sizeof(unit) * 4, Trit val = Unknown);

		TritSet(const TritSet& obj);							//copy constructor
		TritSet(TritSet&& obj) noexcept;						//move constructor

		TritSet& operator=(const TritSet& obj);
		TritSet& operator=(TritSet&& obj) noexcept;

		size_t length() const;									//returns index + 1 of last known trit
		size_t size() const;									//returns number trits
		size_t capacity() const;								//returns number of elements in array
		void shrink();											//frees memory to initial value or to index of last known trit
		void trim(size_t last_index);							//frees memory to last_index inclusively

		int cardinality(Trit val);								//number of trits with value
		std::unordered_map<Trit, int> cardinality();

		

		TritSetReference operator[](size_t i);

		TritSet& operator&=(const TritSet& obj);
		TritSet& operator|=(const TritSet& obj);
		TritSet& operator^=(const TritSet& obj);
		TritSet& flip();

		friend bool operator==(const TritSet &left, const TritSet &right);
	private:
		TritSetData data;
		const size_t initial_trit_num;
	};

	bool operator!=(const TritSet &left, const TritSet &right);
	TritSet operator~(const TritSet &left);
	TritSet operator&(const TritSet &left, const TritSet &right);
	TritSet operator|(const TritSet &left, const TritSet &right);
	TritSet operator^(const TritSet &left, const TritSet &right);

	Trit operator&(const Trit a, const Trit b);
	Trit operator|(const Trit a, const Trit b);
	Trit operator~(const Trit a);
	Trit operator^(const Trit a, const Trit b);
}
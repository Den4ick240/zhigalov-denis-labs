package ru.nsu.ccfit.zhigalov.network;

import ru.nsu.ccfit.zhigalov.network.messages.IMessage;

public interface ConnectionObserver {
    void onConnectionReady(Connection connection);
    void onReceiveObject(Connection connection, IMessage msg);
    void onDisconnect(Connection connection);
    void onException(Connection connection, Exception e);
}

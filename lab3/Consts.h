#pragma once

#include <map>

namespace sea_battle {
    const int FIELD_WIDTH = 10;
    const int FIELD_HEIGHT = 10;
    const std::map<int, int> SHIP_SIZES= {
            {1, 4},
            {2, 3},
            {3, 2},
            {4, 1}
    };

    const int SHIPS_AMOUNT = 10;
    const int SHIP_SIZES_AMOUNT = 4;

}

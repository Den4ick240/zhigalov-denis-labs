package com.zhigalovLabs.Calculator.commands;

public class SumCommand extends BinaryOperationCommand {
    public SumCommand() {
        super(0);
    }

    @Override
    public double operation(double a, double b) {
        return a + b;
    }
}

package com.zhigalovLabs.Calculator.commandsWithLogger;

import com.zhigalovLabs.Calculator.Main;
import com.zhigalovLabs.Calculator.exceptions.NotEnoughCommandArguments;

import java.util.logging.Level;
import java.util.logging.Logger;

public class CommandArgumentSetter {

    protected Logger logger = Main.getLogger();

    private String[] args;
    private final int expectedArgc;
    private static final String UNUSED_ARGUMENTS_WARNING = "Warning: %d of %d arguments are unused for command %s\n";

    public void setArguments(String[] args) throws NotEnoughCommandArguments {
        int argc = args.length - 1;
        if (expectedArgc < argc) {
            System.out.print(argc - expectedArgc);
            String message = String.format(UNUSED_ARGUMENTS_WARNING, argc - expectedArgc, argc, args[0]);
            System.out.format(message);
            logger.info(message);
        }
        if (expectedArgc > argc) {
            var e = new NotEnoughCommandArguments();
            logger.log(Level.WARNING, "", e);
            throw e;
        }
        this.args = args;
    }

    protected CommandArgumentSetter(int argc) {
        expectedArgc = argc;
    }

    protected String[] getArgs() {
        return args;
    }
}

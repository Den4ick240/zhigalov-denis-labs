package com.zhigalovLabs.Calculator.commandsWithLogger;

public class MultiplyCommand extends BinaryOperationCommand {
    public MultiplyCommand() {
        super(0);
    }

    @Override
    public double operation(double a, double b) {
        return a * b;
    }
}

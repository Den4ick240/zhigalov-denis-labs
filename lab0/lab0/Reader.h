#pragma once
#include <fstream>
#include <map>

namespace Zhigalov
{
	class Reader
	{
	public:
		virtual ~Reader();
		void open_file(const std::string & file_name);
		void read_words();
		std::map<std::string, int> get_word_map();

	private:
		std::ifstream in_file;
		std::map<std::string, int> word_map;
	};
}
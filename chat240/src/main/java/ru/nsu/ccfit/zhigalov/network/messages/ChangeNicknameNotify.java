package ru.nsu.ccfit.zhigalov.network.messages;

import ru.nsu.ccfit.zhigalov.network.messages.IMessage;

public class ChangeNicknameNotify extends IMessage {
    String oldNick;

    public ChangeNicknameNotify(String oldNick, String newNick) {
        this.oldNick = oldNick;
        this.newNick = newNick;
    }

    public String getOldNick() {
        return oldNick;
    }

    public String getNewNick() {
        return newNick;
    }

    String newNick;
}

package com.zhigalovLabs.Calculator.exceptions;

public class CalculatorException extends Throwable {
    @Override
    public String getMessage() {
        return "Calculator exception";
    }
}

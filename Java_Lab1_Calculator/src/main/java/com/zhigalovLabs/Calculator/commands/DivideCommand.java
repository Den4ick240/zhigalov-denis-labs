package com.zhigalovLabs.Calculator.commands;

public class DivideCommand extends BinaryOperationCommand{
    public DivideCommand() {
        super(0);
    }

    @Override
    public double operation(double a, double b) {
        return a / b;
    }
}

package com.zhigalovLabs.Calculator.exceptions;

public class UnknownCommand extends CalculatorException {
    private final String name;

    public UnknownCommand(String name) {
        this.name = name;
    }

    @Override
    public String getMessage() {
        return "Unknown command name: " + name;
    }
}

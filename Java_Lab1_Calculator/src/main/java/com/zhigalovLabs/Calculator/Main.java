package com.zhigalovLabs.Calculator;

import com.zhigalovLabs.Calculator.exceptions.ConfigFileException;

import java.io.*;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class Main {
    public final static Logger logger = Logger.getLogger(Main.class.getName());

    public static void setupLogger() {
        LogManager.getLogManager().reset();
        logger.setLevel(Level.ALL);
        try {
            FileHandler fileHandler;
            fileHandler = new FileHandler("main.log");
            logger.addHandler(fileHandler);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Logger getLogger() {
        return logger;
    }

    public static void main(String[] args) {
        InputStreamReader reader = null;
        FileInputStream fileInputStream = null;

        try {
            setupLogger();
            if (args.length == 0) {
                reader = new InputStreamReader(System.in);
                logger.info("no input file");
            } else {
                fileInputStream = new FileInputStream(args[1]);
                reader = new InputStreamReader(fileInputStream);
                logger.info("input file: " + args[1]);
            }

            var controller = new Controller(reader);
            controller.run();
        } catch (ConfigFileException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fileInputStream != null) {
                    fileInputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}

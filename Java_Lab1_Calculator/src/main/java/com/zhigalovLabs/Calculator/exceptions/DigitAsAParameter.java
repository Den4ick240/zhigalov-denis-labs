package com.zhigalovLabs.Calculator.exceptions;

public class DigitAsAParameter extends CalculatorException {
    @Override
    public String getMessage() {
        return "A digit is not a definable parameter name";
    }
}

#include "WorkFlow_general.h"
#include <exception>
#include <string>
#include <map>

#pragma once

namespace workflow {

    namespace exceptions {
        class WorkFlowException : public std::exception {
        protected:
            std::string message;
        public:
            const char *
            what() const _GLIBCXX_TXN_SAFE_DYN _GLIBCXX_USE_NOEXCEPT override;
        };

        class UnexpectedSymbol : public WorkFlowException {
        public:
            explicit UnexpectedSymbol(const std::string &found, const std::string &expected);
        };

        class BlockDefinition : public WorkFlowException {
        public:
            explicit BlockDefinition(int index);
        };

        class BlockType : public BlockDefinition {
        public:
            BlockType(int index, const std::string &type);
        };

       class ArgsAmount : public BlockDefinition {
       public:
            ArgsAmount(int index, std::string type, int neededAmount, int actualAmount);
        };

       class IndexReading : public WorkFlowException {
       public:
           IndexReading();
       };

       class NoIOFile : public WorkFlowException {
       public:
           enum Type { input, output};
           explicit NoIOFile(Type type);
       };

       class UndefinedId : public WorkFlowException{
       public:
            explicit UndefinedId(int id);
       };

       class InvalidChainOrder : public WorkFlowException {
       public:
           InvalidChainOrder();
       };

       class RepeatedId : public WorkFlowException {
       public:
           explicit RepeatedId(int id);
       };

       class InputFileNotFound : public WorkFlowException {
       public:
           explicit InputFileNotFound(const std::string &fileName);
       };

       class WorkflowFileNotFound : public WorkFlowException {
       public:
           explicit WorkflowFileNotFound(const std::string &fileName);
       };
    }
}
package ru.nsu.ccfit.zhigalov.network.messages;

import ru.nsu.ccfit.zhigalov.network.TextMessage;
import ru.nsu.ccfit.zhigalov.network.messages.IMessage;

import java.util.ArrayList;

public class ServerInfo extends IMessage {
    public ServerInfo(ArrayList<TextMessage> messages, String nickname, int id, ArrayList<String> usersOnline) {
        this.messages = messages;
        this.nickname = nickname;
        this.id = id;
        this.usersOnline = usersOnline;
    }

    public ArrayList<TextMessage> getMessages() {
        return messages;
    }

    public String getNickname() {
        return nickname;
    }

    ArrayList<TextMessage> messages;

    public ArrayList<String> getUsersOnline() {
        return usersOnline;
    }

    ArrayList<String> usersOnline;
    String nickname;

    public int getId() {
        return id;
    }

    int id;
}

#include "Writer.h"
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <map>
#include <vector>
#include <algorithm>

namespace
{
	const std::string CSV_SEPARATOR;
}

namespace Zhigalov
{
	using std::string;
	using std::pair;
	using std::map;
	using std::cout;
	using std::endl;

	Writer::~Writer()
	{
		out_file.close();
	}

	void Writer::open_file(const string& file_name)
	{
		out_file.open(file_name);
		if (!out_file.good())
		{
			throw std::exception("Couldn't open output file");
		}
	}

	bool comparator(const pair<string, int>& a, const pair<string, int>& b)
	{
		return a.second > b.second;
	}

	void Writer::count_words(const map<string, int>& word_map)
	{
		word_cnt = 0;
		for (auto p = word_map.cbegin(); p != word_map.cend(); p++)
		{
			word_cnt += p->second;
			word_vector.push_back(*p);
		}
		std::sort(word_vector.begin(), word_vector.end(), comparator);
	}

	void Writer::write_words()
	{
		for (int i = 0; i < word_vector.size(); i++)
		{
			out_file << word_vector[i].first << CSV_SEPARATOR;
			out_file << word_vector[i].second << CSV_SEPARATOR;
			out_file << (double)(word_vector[i].second) * 100 / word_cnt << endl;
		}
	}
}
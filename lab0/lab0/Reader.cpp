#include "Reader.h"
#include <fstream>
#include <string>
#include <iostream>
#include "Parser.h"

namespace Zhigalov
{
	using std::string;
	using std::exception;

	Reader::~Reader()
	{
		in_file.close();
	}

	void Reader::open_file(const string & file_name)
	{
		in_file.open(file_name);
		if (!in_file.good())
		{
			throw exception("Couldn't open input file");
		}
	}

	void Reader::read_words()
	{
		Parser parser(word_map);
		string in_str;

		do {
			if (getline(in_file, in_str).fail())
			{
				throw exception("Couldn't read words");
			}
			parser.parse_string(in_str);
		} while (!in_file.eof());
	}

	std::map<string, int> Reader::get_word_map()
	{
		return word_map;
	}
}
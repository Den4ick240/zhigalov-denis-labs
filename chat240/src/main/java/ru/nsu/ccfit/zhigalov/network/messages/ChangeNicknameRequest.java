package ru.nsu.ccfit.zhigalov.network.messages;

import ru.nsu.ccfit.zhigalov.network.messages.IMessage;

public class ChangeNicknameRequest extends IMessage {
    public ChangeNicknameRequest(String newNickname) {
        this.newNickname = newNickname;
    }

    public String getNewNickname() {
        return newNickname;
    }

    String newNickname;
}

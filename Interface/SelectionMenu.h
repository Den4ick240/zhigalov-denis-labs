#pragma once

#include "InterfaceConsts.h"
#include <set>
#include <string>
#include "GameScene.h"

namespace qt_interface {

    class SelectionMenu : public GameScene {
        Q_OBJECT
    public:
        SelectionMenu(const std::set<std::string> &optionSet, const std::string &labelText);

        virtual ~SelectionMenu();

        std::string &getSelectedOption();

    public slots:
        void buttonPressed();

    signals:
        void optionSelected();

    private:
        QLabel *_label;
        std::vector<QPushButton*> _buttonVector;
        std::set<std::string> _optionSet;
        std::string _selectedOption;

        constexpr static const float WIDTH_RATIO = 0.6;
        constexpr static const float HEIGH_RATIO = 0.6;
    };
}

#include <string>
#include <algorithm>
#include "tritset.h"


namespace tritset
{
	TritSet::TritSet(size_t size, Trit val)
		:data(size, val), initial_trit_num(size)
	{
	}

	TritSet::TritSet(const TritSet & obj)
		:initial_trit_num(obj.initial_trit_num), data(obj.data)
	{
		data.arr = new unit[data.arr_size];
		memcpy(data.arr, obj.data.arr, data.arr_size * sizeof(unit));
	}

	TritSet::TritSet(TritSet && obj) noexcept
		:initial_trit_num(obj.initial_trit_num), data(obj.data)
	{
		obj.data.arr = nullptr;
	}

	TritSet & TritSet::operator=(const TritSet & obj)
	{
		delete[] data.arr;
		data = obj.data;
		data.arr = new unit[data.arr_size];
		memcpy(data.arr, obj.data.arr, data.arr_size * sizeof(unit));
		return *this;
	}

	TritSet & TritSet::operator=(TritSet && obj) noexcept
	{
		delete[] data.arr;
		data = obj.data;
		obj.data.arr = nullptr;
		return *this;
	}

	size_t TritSet::length() const
	{
		for (int i = capacity() - 1; i >= 0; i--)
		{
			if (data.arr[i] != UNKNOWN_UNIT)
			{
				size_t add = (i) * sizeof(unit) * 4;
				size_t j = add + sizeof(unit) * 4 - 1;
				for (j; j >= add; j--)
				{
					if (data.getTrit(j) != Unknown)
					{
						return j + 1;
					}
				}
			}
		}
		return 0;
	}

	size_t TritSet::size() const
	{
		return data.trit_num;
	}

	size_t TritSet::capacity() const
	{
		return data.arr_size;
	}

	TritSetReference TritSet::operator[](size_t i)
	{
		return TritSetReference(data, i);
	}

	void TritSet::shrink()
	{
		data.resize(std::max(initial_trit_num, length()));
	}

	void TritSet::trim(size_t last_index)
	{
		if (last_index <= data.trit_num)
		{
			data.resize(last_index);
		}
	}

	int TritSet::cardinality(Trit val)
	{
		return cardinality()[val];
	}

	std::unordered_map<Trit, int> TritSet::cardinality()
	{
		std::unordered_map<Trit, int> map;
		size_t len = length();
		for (size_t i = 0; i < len; i++)
		{
			Trit a = (*this).data.getTrit(i);
			map[a]++;
		}
		return map;
	}

	TritSet & TritSet::operator&=(const TritSet & right)
	{
		this->data.operator&=(right.data);
		return *this;
	}

	TritSet & TritSet::operator|=(const TritSet & right)
	{
		this->data.operator|=(right.data);
		return *this;
	}
	
	TritSet & TritSet::operator^=(const TritSet & right)
	{
		this->data.operator^=(right.data);
		return *this;
	}

	TritSet & TritSet::flip()
	{
		this->data.flip();
		return *this;
	}

	TritSet operator~(const TritSet & left)
	{
		return TritSet(left).flip();
	}

	TritSet operator&(const TritSet & left, const TritSet & right)
	{
		return TritSet(left) &= right;
	}

	TritSet operator|(const TritSet & left, const TritSet & right)
	{
		return TritSet(left) |= right;
	}

	TritSet operator^(const TritSet & left, const TritSet & right)
	{
		return (TritSet(left).operator^=(right));
	}


	bool operator==(const TritSet & left, const TritSet & right)
	{
		for (size_t i = 0; i < std::min(left.data.arr_size, right.data.arr_size); i++)
		{
			if (left.data.arr[i] != right.data.arr[i])
			{
				return false;
			}
		}
		return true;
	}

	bool operator!=(const TritSet & left, const TritSet & right)
	{
		return !(left == right);
	}

	Trit operator&(const Trit a, const Trit b)
	{
		return Trit(unsigned int(a) & unsigned int(b));
	}

	Trit operator|(const Trit a, const Trit b)
	{
		return Trit(unsigned int(a) | unsigned int(b));
	}

	Trit operator~(const Trit a)
	{
		return Trit(a == Unknown ? Unknown : (~unsigned int(a) & TWO_BITS_TRUE_MASK));
	}

	Trit operator^(const Trit a, const Trit b)
	{
		return Trit((a & ~b) | (~a & b));
	}
}
package ru.nsu.ccfit.zhigalov;

import ru.nsu.ccfit.zhigalov.network.Connection;
import ru.nsu.ccfit.zhigalov.network.ConnectionObserver;
import ru.nsu.ccfit.zhigalov.network.TextMessage;
import ru.nsu.ccfit.zhigalov.network.messages.*;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Server implements ConnectionObserver {
//    private static final String serverHost = "127.0.0.1";
    private static Logger log = Logger.getLogger(Server.class.getName());
    private static final Integer port = 24000;
    private static final Integer THREAD_POOL_CORE_SIZE = 4;

    private final ArrayList<Connection> connections = new ArrayList<>();
    private final ArrayList<TextMessage> textMessageList = new ArrayList<>();
    private final Map<Integer, String> nicknames = new HashMap<>();


    private final static String CONNECTION_FAILURE = "Failed to establish connection with client: ";
    private final static String NICKNAME_ALREADY_EXIST_MESSAGE = "This nickname already exists";
    private final static String INITIAL_NICKNAME = "user";
    private final static String SERVER_MESSAGE_NICKNAME = "server";
    private final static Integer SERVER_MESSAGE_SESSION_ID = -1;
    private final static String CLIENT_CONNECTED_MESSAGE = "Greatings, ";
    private final static String CLIENT_DISCONNECTED_MESSAGE = "Goodbye, ";
    private final static String CLIENT_CHANGED_NICK_FIRST_MESSAGE = "User by the name of ";
    private final static String CLIENT_CHANGED_NICK_SECOND_MESSAGE = " changed his name to ";

    public static void main(String[] arguments) {
        new Server();
    }

    private Server() {
        Connection.setThreadPool((ScheduledThreadPoolExecutor) Executors.newScheduledThreadPool(THREAD_POOL_CORE_SIZE));
        log.setLevel(Level.INFO);
        log.info("Server started");

        try (ServerSocket serverSocket = new ServerSocket(port)) {
            while (true) {
                try {
                    new Connection(this, serverSocket.accept());
                } catch (IOException e) {
                    log.warning(CONNECTION_FAILURE + e);
                }
            }
        } catch (IOException e) {
            log.severe(e.getMessage());
        }
    }

    private void broadcast(IMessage msg) {
        for (var client : connections) {
            client.sendObject(msg);
        }
    }

    private void broadcastServerMessage(String text) {
        var msg = new TextMessage(SERVER_MESSAGE_NICKNAME, text, SERVER_MESSAGE_SESSION_ID);
        textMessageList.add(msg);
        broadcast(new NewTextMessage(msg));
    }

    @Override
    public synchronized void onConnectionReady(Connection connection) {
        connections.add(connection);
        log.info("New connection: " + connection.toString());
        String nickname = INITIAL_NICKNAME + connection.getId().toString();
        nicknames.put(connection.getId(), nickname);
        var usersOnline = getUsersOnline();
        usersOnline.remove(nickname);
        connection.sendObject(new ServerInfo(textMessageList, nickname, connection.getId(), usersOnline));
        broadcast(new ClientConnected(nickname));
        broadcastServerMessage(CLIENT_CONNECTED_MESSAGE + nickname);
    }

    private ArrayList<String> getUsersOnline() {
        var out = new ArrayList<String>();
        for (var nick : nicknames.entrySet()) {
            out.add(nick.getValue());
        }
        return out;
    }

    @Override
    public synchronized void onReceiveObject(Connection connection, IMessage msg) {
        Class msgClass = msg.getClass();

        if (msgClass.equals(Disconnect.class)) {
            onDisconnect(connection);
            return;
        }
        if (msgClass.equals(ChangeNicknameRequest.class)) {
            String newNickname = ((ChangeNicknameRequest) msg).getNewNickname();
            if (nicknames.containsValue(newNickname)) {
                var oldNickname = nicknames.get(connection.getId());
                connection.sendObject(new ChangeNicknameResponse(false, NICKNAME_ALREADY_EXIST_MESSAGE, oldNickname));
            } else {
                String oldNick = nicknames.get(connection.getId());
                nicknames.put(connection.getId(), newNickname);
                connection.sendObject(new ChangeNicknameResponse(true, null, newNickname));
                broadcast(new ChangeNicknameNotify(oldNick, newNickname));
                broadcastServerMessage(CLIENT_CHANGED_NICK_FIRST_MESSAGE + oldNick + CLIENT_CHANGED_NICK_SECOND_MESSAGE + newNickname);
            }
            return;
        }

        if (msgClass.equals(NewTextMessage.class)) {
            textMessageList.add(((NewTextMessage) msg).getMessage());
            broadcast(msg);
            log.info("Message received from " + connection.toString());
            return;
        }


    }

    @Override
    public synchronized void onDisconnect(Connection connection) {
        int id = connection.getId();
        String nickname = nicknames.get(id);
        log.info("Connection closed: " + connection.toString());
        connection.disconnect();
        nicknames.remove(id);
        connections.remove(connection);
        broadcast(new ClientDisconnected(nickname));
        broadcastServerMessage(CLIENT_DISCONNECTED_MESSAGE + nickname);
    }

    @Override
    public synchronized void onException(Connection connection, Exception e) {
        log.warning(connection.toString() + " " + e.toString());
        e.printStackTrace();
    }
}

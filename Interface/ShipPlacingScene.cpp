#include "ShipPlacingScene.h"


namespace qt_interface {

    ShipPlacingScene::ShipPlacingScene(const sea_battle::OwnField *field)
        : _field(field), _ship(0)
    {
        int fieldSize = VIEW_WIDTH * FIELD_SIZE_RATIO;
        int fieldOffset = fieldSize * FIELD_OFFSET_RATIO;
        _fieldItem = new ClickableField<sea_battle::OwnField>(field, fieldSize, fieldOffset, fieldOffset);
        _fieldItem->addToScene(this);

        int buttonPlaceSize = fieldSize / field->getUnplacedShips().size();
        int buttonHeight = buttonPlaceSize * BUTTON_SIZE_RATIO;
        int buttonOffset = (buttonPlaceSize - buttonHeight) / 2;

        int buttonId = 0;

        for (auto [shipSize, amount] : field->getUnplacedShips()) {
            auto *btn = new QPushButton();

            QPixmap pixmap(SHIP_IMAGE.at(shipSize));
            pixmap = pixmap.scaledToHeight(buttonHeight);
            QIcon icon(pixmap);
            btn->setIcon(icon);
            btn->setIconSize(pixmap.rect().size());
            btn->setStyleSheet("background : transparent");
            btn->resize(pixmap.rect().size());

            int pos_x = VIEW_WIDTH - fieldOffset - btn->width();
            int pos_y = fieldOffset + buttonOffset + buttonId * buttonPlaceSize;
            btn->move(pos_x, pos_y);
            buttonId++;

            this->addWidget(btn);

            _resizeButtons[shipSize] = btn;

            connect(btn, SIGNAL(clicked()), this, SLOT(resizeButtonPressed()));
        }

        _fieldItem->connectButtonsToSlot(this, SLOT(selectPlace(Qt::MouseButton, int, int)));

        _shipCursor = new QGraphicsPixmapItem();
        this->addItem(_shipCursor);
    }

    void ShipPlacingScene::refreshButtons()
    {
        auto unplacedShips = _field->getUnplacedShips();
        for (auto [size, button] : _resizeButtons) {
            if (unplacedShips.find(size) == unplacedShips.end()) {
                button->setIcon(QIcon());
                disconnect(button, SIGNAL(clicked()), this, SLOT(resizeButtonPressed()));
                if (size == _ship.size()) {
                    _shipCursor->setRotation(0);
                    _shipCursor->setPixmap(QPixmap());
                    _ship = sea_battle::Ship(0);
                }
            }
        }


        _fieldItem->refresh();
    }

    sea_battle::Ship ShipPlacingScene::getShip() const
    {
        return _ship;
    }

    void ShipPlacingScene::refreshShipRotation()
    {
        if (_ship.orientation() == sea_battle::Ship::Orientation::vertical) {
            int pointCoord = _fieldItem->cellSize() / 2;
            _shipCursor->setTransformOriginPoint(pointCoord, pointCoord);
            _shipCursor->setRotation(90);
        }
        else {
            _shipCursor->setRotation(0);
        }
    }

    void ShipPlacingScene::resizeButtonPressed()
    {
        for (auto [key, value] : _resizeButtons) {
            if (value == sender()) {
                _ship = sea_battle::Ship(key);
                refreshShipRotation();
                QPixmap pixmap = QPixmap(SHIP_IMAGE.at(key));
                pixmap = pixmap.scaledToHeight(_fieldItem->cellSize());
                _shipCursor->setPixmap(pixmap);
                return;
            }
        }
    }

    void ShipPlacingScene::selectPlace(Qt::MouseButton button, int x, int y)
    {
        if (button == Qt::LeftButton && _ship.size() != 0) {
            _ship.move(x, y);
            emit placeSelected();
        }
    }

    void ShipPlacingScene::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
    {
        QGraphicsScene::mouseMoveEvent(event);
        auto point = event->scenePos();
        int offset = _fieldItem->cellSize() / 2;
        point.rx() -= offset;
        point.ry() -= offset;
        _shipCursor->setPos(point);
    }

    void ShipPlacingScene::mousePressEvent(QGraphicsSceneMouseEvent *event)
    {
        QGraphicsScene::mousePressEvent(event);
        if (event->button() == Qt::RightButton) {
            _ship.rotate();
            refreshShipRotation();
        }
    }

}

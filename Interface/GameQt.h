#pragma once

#include "InterfaceConsts.h"
#include "GameInterface.h"
#include "../lab3/Game.h"
#include "ShipPlacingScene.h"
#include "SelectionMenu.h"
#include "BattleScene.h"

namespace qt_interface {
    class GameQt : public QObject, public sea_battle::Game {
    Q_OBJECT
    public:
        explicit GameQt(QGraphicsView *view);

        void arrangeShips() override;

        void endGame() override;

        void run();

    public slots:
        void frame();

        void exit();

    protected:
        QTimer *_timer;
        QGraphicsView *_view;
        GameInterface *_interfaceQt;
        BattleScene *_battleScene;

        static const int TIMER_DELAY = 1;
    };
}

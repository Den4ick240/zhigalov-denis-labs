package com.zhigalovLabs.Calculator.commands;

import com.zhigalovLabs.Calculator.CommandContext;
import com.zhigalovLabs.Calculator.exceptions.CalculatorException;

public interface ICommand {
    public void run(CommandContext context) throws CalculatorException;
}

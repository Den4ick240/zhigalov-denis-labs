package com.zhigalovLabs.Calculator.exceptions;

public class EmptyStack extends CalculatorException{
    @Override
    public String getMessage() {
        return "Empty stack";
    }
}

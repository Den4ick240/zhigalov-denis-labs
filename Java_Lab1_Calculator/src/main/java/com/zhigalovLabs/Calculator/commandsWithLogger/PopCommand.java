package com.zhigalovLabs.Calculator.commandsWithLogger;

import com.zhigalovLabs.Calculator.CommandContext;
import com.zhigalovLabs.Calculator.commands.ICommand;

public class PopCommand extends CommandArgumentSetter implements ICommand {
    public PopCommand() {
        super(0);
    }
    @Override
    public void run(CommandContext context) {
        double val = context.stackPop();
        logger.info("Value was popped from stack: " + Double.toString(val));
    }
}

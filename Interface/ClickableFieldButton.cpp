#include "ClickableFieldButton.h"
#include "InterfaceConsts.h"

namespace qt_interface {

    ClickableFieldButton::ClickableFieldButton(int x, int y)
        : onField_x(x), onField_y(y) {
        setStyleSheet(FIELD_BUTTON_STYLE_SHEET);
    }

    void ClickableFieldButton::mousePressEvent(QMouseEvent *e)
    {
        emit Clicked(e->button(), onField_x, onField_y);
    }

}

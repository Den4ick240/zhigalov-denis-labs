import com.zhigalovLabs.Calculator.Controller;
import com.zhigalovLabs.Calculator.exceptions.CalculatorException;
import com.zhigalovLabs.Calculator.exceptions.ConfigFileException;
import com.zhigalovLabs.Calculator.exceptions.NotEnoughCommandArguments;
import com.zhigalovLabs.Calculator.exceptions.UnknownCommand;
import org.junit.Test;

import java.io.InputStreamReader;

public class ControllerTest {
    private Controller controller;
    public ControllerTest() throws ConfigFileException {
        controller = new Controller(new InputStreamReader(System.in));
    }
    @Test(expected = UnknownCommand.class)
    public void UnknownCommandTest() throws CalculatorException {
        String line = "ABSOLUTELY_NOT_A_COMMAND_NAME NOT_AN_ARGUMENT";
        controller.executeLine(line);
    }
    @Test(expected = NotEnoughCommandArguments.class)
    public void NotEnoughCommandArgumentsTest() throws CalculatorException {
        String line = "PUSH";
        controller.executeLine(line);
    }
}

package com.zhigalovLabs.Calculator.commandsWithLogger;

import com.zhigalovLabs.Calculator.CommandContext;
import com.zhigalovLabs.Calculator.commands.ICommand;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.EmptyStackException;

public class PrintCommand extends CommandArgumentSetter implements ICommand {
    public PrintCommand() {
        super(0);
    }
    @Override
    public void run(CommandContext context) throws EmptyStackException {
        double val = context.stackPeek();
        System.out.println(val);
        logger.info("Value from stack printed: " + Double.toString(val));
    }
}

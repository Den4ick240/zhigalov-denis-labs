#include "Field.h"

namespace sea_battle {

    FieldData::FieldData() {
        clear();
    }

    bool FieldData::allShipsDead() {
        return _deadShips.size() == _allShips.size();
    }

    void FieldData::clear() {
        for (int y = 0; y < FIELD_HEIGHT; y++) {
            for (int x = 0; x < FIELD_WIDTH; x++) {
                changeCell(x, y, CellState::clear);
            }
        }
    }

    CellState FieldData::getCell(int x, int y) const {
        return _cellsField[y][x];
    }

    void FieldData::changeCell(int x, int y, CellState state) {
        _cellsField[y][x] = state;
    }

    CellState EnemyField::getCell(int x, int y) const {
        CellState state = FieldData::getCell(x, y);
        if (state == CellState::alive) {
            return CellState::clear;
        }
        return state;
    }

    const std::vector<Ship> &EnemyField::getShips() const {
        return _deadShips;
    }

    void EnemyField::tryExclude(int x, int y) {
        if (FieldData::getCell(x, y) != CellState::clear) {
            return;
        }

        for (int near_y = std::max(0, y - 1); near_y < std::min(FIELD_HEIGHT, y + 2); near_y++) {
            for (int near_x = std::max(0, x - 1); near_x < std::min(FIELD_WIDTH, x + 2); near_x++) {

                CellState state = FieldData::getCell(near_x, near_y);
                if (state == CellState::dead) {
                    changeCell(x, y, CellState::excluded);
                    return;
                }
            }
        }
    }

    CellState OwnField::getCell(int x, int y) const {
        CellState state = FieldData::getCell(x, y);

        if (state == CellState::excluded) {
            return CellState::clear;
        }
        return state;
    }

    const std::vector<Ship> &OwnField::getShips() const {
        return _allShips;
    }

    bool OwnField::tryToAddShip(Ship ship) {
        if (!ship.doesFitIntoField()) {
            return false;
        }
        for (auto otherShip : _allShips) {
            if (intersect(ship, otherShip)) {
                return false;
            }
        }
        auto iter = unplacedShips.find(ship.size());
        if (iter == unplacedShips.end()) {
            return false;
        }
        iter->second = iter->second - 1;
        if (iter->second == 0) {
            unplacedShips.erase(iter);
        }

        _allShips.push_back(ship);

        ship.foreachCell([this](int x, int y) -> void {
            changeCell(x, y, CellState::alive);
        });

        return true;
    }

    OwnField::OwnField(std::map<int, int> shipsToPlace)
            : unplacedShips(std::move(shipsToPlace)) {

    }

    OwnField::OwnField()
            : unplacedShips(SHIP_SIZES) {}

    const std::map<int, int> &OwnField::getUnplacedShips() const {
        return unplacedShips;
    }

    bool EnemyField::shot(int x, int y) {
        CellState state = FieldData::getCell(x, y);
        if (state == CellState::clear) {
            changeCell(x, y, CellState::miss);
            return false;
        }
        if (state != CellState::alive) {
            return true;
        }

        changeCell(x, y, CellState::hit);
        Ship ship = *std::find_if(_allShips.begin(), _allShips.end(),
                                  [x, y](const Ship &ship) {
                                      return ship.doesCoverCell(x, y);
                                  });

        bool allHitFlag = true;
        ship.foreachCell([&allHitFlag, this](int x, int y) -> void {
            if (FieldData::getCell(x, y) != CellState::hit) {
                allHitFlag = false;
            }
        });

        if (allHitFlag) {
            ship.foreachCell([this](int x, int y) -> void {
                changeCell(x, y, CellState::dead);
            });
            _deadShips.push_back(ship);
        }
        return true;
    }

}
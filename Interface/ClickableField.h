#pragma once
#include "GraphicsField.h"
#include "ClickableFieldButton.h"


namespace qt_interface {

    template<class fieldType>
    class ClickableField : public GraphicsField<fieldType> {
        typedef GraphicsField<fieldType> BaseClass;

    public:
        ClickableField(const fieldType *field, qreal size, qreal x = 0, qreal y = 0, FieldRefreshMode refreshMode = FieldRefreshMode::refreshAll)
            : GraphicsField<fieldType>(field, size, x, y, refreshMode) {

            BaseClass::foreachCell([this](QGraphicsPixmapItem *, int x, int y) {
                _cellButtonField[y][x] = new ClickableFieldButton(x, y);
                _cellButtonField[y][x]->setStyleSheet(FIELD_BUTTON_STYLE_SHEET);
            });
            setButtons();
        }

        void setField(qreal x, qreal y, qreal size)
        {
            BaseClass::setField(x, y, size);
            setButtons();
        }

        void addToScene(QGraphicsScene *scene)
        {
            BaseClass::addToScene(scene);
            BaseClass::foreachCell([this, scene](QGraphicsPixmapItem *, int x, int y) {
                scene->addWidget(_cellButtonField[y][x]);
            });
        }

        template<class objType>
        void connectButtonsToSlot(objType *obj, const char* slot) {
            BaseClass::foreachCell([this, obj, slot](QGraphicsPixmapItem *, int x, int y) {
                obj->connect(_cellButtonField[y][x], SIGNAL(Clicked(Qt::MouseButton, int, int)), obj, slot);
            });
        }

        struct fieldPressedInfo {
            Qt::MouseButton button;
            int x, y;
        };

    protected:
        ClickableFieldButton *_cellButtonField[FIELD_CELLS_NUM][FIELD_CELLS_NUM];

        void setButtons() {
            BaseClass::foreachCell([this](QGraphicsPixmapItem *, int x, int y) {
                auto [pos_x, pos_y] = BaseClass::convertCellCoordFromLogicFieldToPixel(x, y);
                pos_x += this->x();
                pos_y += this->y();
                _cellButtonField[y][x]->move(pos_x, pos_y);
                _cellButtonField[y][x]->resize(BaseClass::_cellSize, BaseClass::_cellSize);
            });
        }
    };

}

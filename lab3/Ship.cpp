#include "Ship.h"

namespace sea_battle {

    Ship::Ship(uint8_t size)
        : _size(size)/*, hits(size)*/ {
//        for (auto hit : hits) {
//            hit = false;
//        }
    }

    bool Ship::doesCoverCell(int x, int y) const {
        if (x < _x || y < _y) {
            return false;
        }
        if (_orientation == Orientation::vertical &&
            x == _x && y < (_y + _size)) {
            return true;
        }
        if (_orientation == Orientation::horizontal &&
            y == _y && x < (_x + _size)) {
            return true;
        }
        return false;
    }

    void Ship::move(uint8_t x, uint8_t y) {
        _x = x;
        _y = y;
    }

    void Ship::rotate() {
        _orientation =
                (_orientation == Orientation::horizontal ?
                Orientation::vertical : Orientation::horizontal);
    }

    bool intersect(Ship &a, Ship &b) {
        if (a._orientation == Ship::Orientation::horizontal) {
            for (int x = - 1; x < a._size + 1; x++) {
                for (int y = - 1; y < 2; y++) {
                    if (b.doesCoverCell(a._x + x, a._y + y)) {
                        return true;
                    }
                }
            }
            return false;
        }
        for (int x = - 1; x < 2; x++) {
            for (int y = - 1; y < a._size + 1; y++) {
                if (b.doesCoverCell(a._x + x, a._y + y)) {
                    return true;
                }
            }
        }
        return false;
    }

    int Ship::size() const {
        return _size;
    }

    bool Ship::doesFitIntoField() const{
        if (_orientation == Orientation::horizontal &&
            _x <= FIELD_WIDTH - _size && _y < FIELD_HEIGHT) {
            return true;
        }
        if (_orientation == Orientation::vertical &&
            _y <= FIELD_WIDTH - _size && _x < FIELD_HEIGHT) {
            return true;
        }
        return false;
    }

    void Ship::foreachCell(std::function<void(int x, int y)> body) {
        int x = _x;
        int y = _y;
        for (int i = 0; i < _size; i++) {
            body(x, y);
            if (_orientation == Ship::Orientation::vertical) {
                y++;
            }
            else {
                x++;
            }
        }
    }

    int Ship::x() const {
        return _x;
    }

    int Ship::y() const {
        return _y;
    }

    Ship::Orientation Ship::orientation() const {
        return _orientation;
    }


}

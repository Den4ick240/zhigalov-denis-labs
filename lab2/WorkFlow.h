#include "creator.h"
#include "parser.h"
#include <vector>

#pragma once

namespace workflow {

    class Executor {
    public:
        explicit Executor(IBlockCreator *ibLockCreator);

        void execute(IParser *parser, const std::string &inputFileName = std::string(),
                     const std::string &outputFileName = std::string());

    private:
        std::vector<std::string> text;
        IBlockCreator *blockCreator;
    };

}
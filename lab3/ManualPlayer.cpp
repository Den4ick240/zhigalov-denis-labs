#include "ManualPlayer.h"
#include "Consts.h"

namespace sea_battle {

    ManualPlayer::ManualPlayer(OwnField &ownField, EnemyField &enemyField, IGameInterface &interface)
            : IPlayer(ownField, enemyField), _interface(interface) {
    }

    void ManualPlayer::placeShips() {
        auto unplacedShips = SHIP_SIZES;

        while (!unplacedShips.empty()) {
            Ship ship = _interface.getNextShipToPlace(unplacedShips);

            if (_ownField.tryToAddShip(ship)) {
                unplacedShips[ship.size()]--;

                if (unplacedShips[ship.size()] == 0) {
                    unplacedShips.erase(ship.size());
                }
            }
        }
    }

    bool ManualPlayer::move() {
        auto move = _interface.getNextMove(_ownField, _enemyField);
        if (move.moveType == move.exclude) {
            _enemyField.tryExclude(move.x, move.y);
            return true;
        } else {
            return _enemyField.shot(move.x, move.y);
        }
    }

}


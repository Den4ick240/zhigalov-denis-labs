package com.zhigalovLabs.Calculator.commands;

import com.zhigalovLabs.Calculator.CommandContext;
import com.zhigalovLabs.Calculator.exceptions.CalculatorException;

public class PushCommand extends CommandArgumentSetter implements ICommand {
    public PushCommand() {
        super(1);
    }

    @Override
    public void run(CommandContext context) throws CalculatorException {
        double val;
        try {
            val = Double.parseDouble(getArgs()[1]);
        } catch (NumberFormatException e) {
            val = context.getParameter(getArgs()[1]);
        }
        context.stackPush(val);
    }
}

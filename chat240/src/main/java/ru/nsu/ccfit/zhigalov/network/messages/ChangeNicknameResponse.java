package ru.nsu.ccfit.zhigalov.network.messages;

import ru.nsu.ccfit.zhigalov.network.messages.IMessage;

public class ChangeNicknameResponse extends IMessage {
    boolean changed;

    public ChangeNicknameResponse(boolean changed, String reason, String nickname) {
        this.changed = changed;
        this.reason = reason;
        this.nickname = nickname;
    }

    public boolean isChanged() {
        return changed;
    }

    public String getReason() {
        return reason;
    }

    public String getNickname() {
        return nickname;
    }

    String nickname;
    String reason;
}

#include "chainParser.h"

namespace workflow {
    ChainParser::ChainParser(std::istream &inputStream)
            : inputStream(inputStream) {
    }

    IChainParser &ChainParser::operator++() {
        if (!finished) {
            parseNext();
        }
        return *this;
    }

    int ChainParser::operator*() {
        return val;
    }

    void ChainParser::parseIndex() {
        if (inputStream.eof()) {
            throw exceptions::IndexReading();
        }
        inputStream >> val;
        if (inputStream.fail()) {
            throw exceptions::IndexReading();
        }
    }

    void ChainParser::parseNext() {
        if (inputStream.eof()) {
            finished = true;
            return;
        }
        std::string word;
        inputStream >> word;
        if (word.empty()) {
            finished = true;
            return;
        }
        if (word != CHAIN_KEY_WORD) {
            throw exceptions::UnexpectedSymbol(word, CHAIN_KEY_WORD);
        }
        parseIndex();
    }

    bool ChainParser::is_finished() {
        return finished;
    }
}
package com.zhigalovLabs.Calculator.commandsWithLogger;

import com.zhigalovLabs.Calculator.CommandContext;
import com.zhigalovLabs.Calculator.commands.ICommand;

import java.util.EmptyStackException;

public abstract class BinaryOperationCommand extends CommandArgumentSetter implements ICommand {
    public BinaryOperationCommand(int argc) {
        super(argc);
    }

    public abstract double operation(double a, double b);

    @Override
    public void run(CommandContext context) throws EmptyStackException {
        double a, b, result;
        b = context.stackPop();
        try {
            a = context.stackPop();
        } catch (EmptyStackException e) {
            context.stackPush(b);
            logger.warning("Not enough elements on the stack for binary operation");
            throw e;
        }
        result = operation(a, b);
        context.stackPush(result);
        logger.info(String.format("Binary operation %s; Arguments %f, %f; Result %f", getArgs()[0], a, b, result));
    }
}

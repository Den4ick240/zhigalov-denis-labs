#include "GameQt.h"

namespace qt_interface {

    GameQt::GameQt(QGraphicsView *view)
        : sea_battle::Game(_interfaceQt = new GameInterface(view)), _view(view) {

        view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        view->setFixedSize(qt_interface::VIEW_WIDTH, qt_interface::VIEW_HEIGH);
        view->setBackgroundBrush(qt_interface::BACKGROUND_BRUSH);

        _timer = new QTimer();
    }

    void GameQt::arrangeShips()
    {
        auto *scene = new ShipPlacingScene(_fields[0]);
        _interfaceQt->setPlacingScene(scene);
        _view->setScene(scene);


        sea_battle::Game::arrangeShips();

        delete scene;
    }

    void GameQt::run()
    {
        createPlayers();
        arrangeShips();

        _battleScene = new BattleScene(_fields[0], _fields[1]);
        _battleScene->connectToExitButton(this, SLOT(exit()));
        _view->setScene(_battleScene);
        _interfaceQt->setBattleScene(_battleScene);
        connect(_timer, SIGNAL(timeout()), this, SLOT(frame()));
        _timer->start(TIMER_DELAY);

    }

    void GameQt::endGame()
    {
        int winnerId;
        if (_fields[0]->allShipsDead()) {
            winnerId = 1;
        }
        else {
            winnerId = 0;
        }
        _battleScene->endGame(_playerNames[winnerId], winnerId);
    }

    void GameQt::frame()
    {
        bool gameEnded = !gameFrame();
        _battleScene->refresh();
        if (gameEnded) {
            _timer->stop();
            endGame();
        }
    }

    void GameQt::exit()
    {
        QApplication::quit();
    }

}

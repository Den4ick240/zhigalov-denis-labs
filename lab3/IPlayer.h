#pragma once

#include "Field.h"

namespace sea_battle {

    class IPlayer {
    public:
        virtual void placeShips() = 0;

        virtual bool move() = 0;

        bool loosed();

        virtual ~IPlayer() = default;

        const OwnField *getOwnField();

        const EnemyField *getEnemyField();

    protected:
        IPlayer(OwnField &ownField, EnemyField &enemyField);

        OwnField &_ownField;
        EnemyField &_enemyField;
    };
}

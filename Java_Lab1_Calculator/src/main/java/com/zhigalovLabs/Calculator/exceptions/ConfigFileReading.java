package com.zhigalovLabs.Calculator.exceptions;

public class ConfigFileReading extends ConfigFileException {
    @Override
    public String getMessage() {
        return "Couldn't read config file";
    }
}

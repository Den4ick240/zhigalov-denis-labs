#pragma once

#include <map>
#include <vector>
#include <string>
#include <functional>
#include <cstdint>
#include <set>

#include "Field.h"
#include "IPlayer.h"
#include "IGameInterface.h"
#include "ManualPlayer.h"
#include "RandomBotPlayer.h"
#include "SmartBotPlayer.h"

namespace sea_battle {

    class Game {
        static const std::vector<std::string> PlayerTypeNames;
        static const std::set<std::string> POSSIBLE_FIRST_PLAYERS;
        static const std::set<std::string> POSSIBLE_SECOND_PLAYERS;
        const std::map<std::string, std::function<IPlayer *(OwnField &, EnemyField &)>> playerTypeAndCreatorMap = {
                {"Manual player", [this](OwnField &ownField, EnemyField &enemyField) -> IPlayer * {
                    return new ManualPlayer(ownField, enemyField, *_interface);
                }},
                {"Random bot",    [](OwnField &ownField, EnemyField &enemyField) -> IPlayer * {
                    return new RandomBotPlayer(ownField, enemyField);
                }},
                {"Smart bot",     [](OwnField &ownField, EnemyField &enemyField) -> IPlayer * {
                    return new SmartBotPlayer(ownField, enemyField);
                }}
        };

    public:

        explicit Game(IGameInterface *interface);

        virtual void createPlayers();

        virtual void arrangeShips();

        virtual bool gameFrame();

        virtual void endGame();

        virtual ~Game();

    protected:
        static const int PLAYERS_NUM = 2;
        IGameInterface *_interface;
        std::string _playerNames[PLAYERS_NUM] = {"Player A", "Player B"};
        IPlayer *_players[PLAYERS_NUM];
        OwnField *_fields[PLAYERS_NUM];
    };

}
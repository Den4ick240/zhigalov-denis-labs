#pragma once
#include <string>
#include <map>

namespace Zhigalov
{
	class Parser
	{
	public:
		Parser(std::map<std::string, int> &word_map);
		void parse_string(const std::string& str);
	private:
		bool is_separator(char a);
		std::map<std::string, int> &word_map;
	};
}
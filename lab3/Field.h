#pragma once

#include <vector>
#include <map>

#include "Ship.h"

namespace sea_battle {

    enum class CellState {
        clear = 0,
        miss,
        hit,
        dead,
        excluded,
        alive
    };

    class FieldData {
    public:
        FieldData();

        bool allShipsDead();

        void clear();

        virtual ~FieldData() = default;

    protected:

        virtual void changeCell(int x, int y, CellState state);

        CellState getCell(int x, int y) const;

        std::vector<Ship> _allShips;
        std::vector<Ship> _deadShips;

    private:
        CellState _cellsField[FIELD_HEIGHT][FIELD_WIDTH];
    };


    class EnemyField : public FieldData { //This class gives access to the field for enemy
    public:
        CellState getCell(int i, int j) const;

        const std::vector<Ship> &getShips() const;

        void tryExclude(int x, int y);

        bool shot(int x, int y); //returns false if missed, true if hit or an invalid move

    protected:
        EnemyField() = default;

    };

    class OwnField : public EnemyField {
    public:
        explicit OwnField(std::map<int, int> shipsToPlace);

        OwnField();

        CellState getCell(int x, int y) const;

        const std::vector<Ship> &getShips() const;

        bool tryToAddShip(Ship ship);

        const std::map<int, int> &getUnplacedShips() const;

    private:
        std::map<int, int> unplacedShips;
    };

}

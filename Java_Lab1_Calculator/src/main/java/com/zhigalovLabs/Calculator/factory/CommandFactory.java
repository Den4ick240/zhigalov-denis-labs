package com.zhigalovLabs.Calculator.factory;

import com.zhigalovLabs.Calculator.Main;
import com.zhigalovLabs.Calculator.commands.CommandArgumentSetter;
import com.zhigalovLabs.Calculator.commands.ICommand;
import com.zhigalovLabs.Calculator.exceptions.*;

import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CommandFactory {
    private final Logger logger = Main.getLogger();
    private static final String configFileName = "/factoryConfig.txt";
    private Map<String, Class> commandsMap;

    public CommandFactory() throws ConfigFileException {
        InputStream inputStream = CommandFactory.class.getResourceAsStream(configFileName);
        if (inputStream == null) {
            throw new ConfigFileNotFound();
        }
        ConfigReader configReader = new ConfigReader(inputStream);
        commandsMap = configReader.readCommands();
    }

    public ICommand createCommand(String[] arguments) throws CalculatorException, CommandLoading {
        String commandName = arguments[0];
        CommandArgumentSetter commandArgumentSetter = null;
        if (!commandsMap.containsKey(commandName)) {
            throw new UnknownCommand(commandName);
        }
        try {
            commandArgumentSetter = (CommandArgumentSetter) commandsMap.get(commandName).getDeclaredConstructor().newInstance();
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException | InstantiationException | ClassCastException e) {
            logger.log(Level.SEVERE, "COULDN'T GET INSTANCE OF COMMAND CLASS: " + commandsMap.get(commandName).getName(), e);
            throw new CommandLoading(commandName);
        }

        commandArgumentSetter.setArguments(arguments);
        ICommand out = (ICommand) commandArgumentSetter;
        return out;
    }
}


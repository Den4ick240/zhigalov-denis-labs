import com.zhigalovLabs.Calculator.CommandContext;
import com.zhigalovLabs.Calculator.exceptions.EmptyStack;
import com.zhigalovLabs.Calculator.exceptions.UndefinedParameter;
import org.junit.*;
import org.junit.Test;

import java.util.EmptyStackException;
import java.util.Stack;


public class CommandContextTest {
    //    @Test
//    public void stackTest() {
//        Double[] testArr = {4.3, 3.4, 23.2, 32234.44234, -2234.2 };
//        Stack<Double> verificationStack = new Stack<>();
//        CommandContext commandContext = new CommandContext();
//
//        for (int i = testArr.length; i > 0; i--) {
//            for (int j = 0; j < i; j++) {
//                commandContext.stackPush((double)testArr[j]);
//                verificationStack.push()
//            }
//        }
//
//        commandContext.stackPush((double)4);
//        Assert.assertEquals((double)commandContext.stackPeek(), (double)4);
//        Assert.assertEquals((double)commandContext.stackPeek(), (double)4);
//    }
    @Test(expected = EmptyStackException.class)
    public void emptyStackTest1() {
        var commandContext = new CommandContext();
        commandContext.stackPop();
    }

    @Test(expected = EmptyStackException.class)
    public void emptyStackTest2() {
        var commandContext = new CommandContext();
        commandContext.stackPeek();
    }

    @Test(expected = EmptyStackException.class)
    public void emptyStackTest3() {
        var commandContext = new CommandContext();
        commandContext.stackPush(2.0);
        commandContext.stackPop();
        commandContext.stackPop();
    }

    @Test(expected = UndefinedParameter.class)
    public void undefinedParameterException() throws UndefinedParameter {
        var commandContext = new CommandContext();
        commandContext.getParameter("a");
    }

    @Test
    public void parameterDefiningTest() throws UndefinedParameter {
        var commandContext = new CommandContext();
        commandContext.setParameter("a", 2.0);
        Assert.assertEquals((double)commandContext.getParameter("a"), 2.0, 0.0000001);
    }
}

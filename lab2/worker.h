#include <string>
#include <vector>

#pragma once

namespace workflow {
    enum class BlockType {
        input, output, worker
    };

    class IBlock {
    public:
        const BlockType blockType;

        virtual void DoWork(std::vector<std::string> &text) = 0;

        virtual ~IBlock() = default;
    protected:
        explicit IBlock(BlockType blockType);
    };

    class Readfile : public IBlock {
    public:
        explicit Readfile(std::string fileName);

        void DoWork(std::vector<std::string> &text) override;

    private:
        const std::string fileName;
    };

    class Writefile : public IBlock {
    public:
        explicit Writefile(std::string fileName);

        void DoWork(std::vector<std::string> &text) override;

    private:
        const std::string fileName;
    };

    class Grep : public IBlock {
    public:
        explicit Grep(std::string word);

        void DoWork(std::vector<std::string> &text) override;

    private:
        const std::string word;
    };

    class Sort : public IBlock {
    public:
        Sort();

        void DoWork(std::vector<std::string> &text) override;

    private:
    };

    class Replace : public IBlock {
    public:
        Replace(std::string word1, std::string word2);

        void DoWork(std::vector<std::string> &text) override;

    private:
        const std::string word1, word2;
    };

    class Dump : public IBlock {
    public:
        explicit Dump(std::string fileName);

        void DoWork(std::vector<std::string> &text) override;

    private:
        const std::string fileName;
    };
}

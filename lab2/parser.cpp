#include "parser.h"

#include <utility>
#include <sstream>
#include "exceptions.h"

namespace workflow {

    Parser::Parser(std::istream &inputStream)
            : chainParser(new ChainParser(inputStream)), inputStream(inputStream) {
        std::string buff;

        while (buff != OPEN_KEY_WORD) {
            std::getline(inputStream, buff);

            if (inputStream.eof()) {
                throw exceptions::UnexpectedSymbol(EOF_MESSAGE, OPEN_KEY_WORD);
            }
        }
    }

    void Parser::ParseBlocks(
            std::function<void(const int &, const std::string &, const std::vector<std::string> &)> addBlock) {
        std::string buffer;

        while (true) {
            try {
                std::getline(inputStream, buffer);
            }
            catch (std::exception &e) {
                throw exceptions::UnexpectedSymbol(EOF_MESSAGE, OPEN_KEY_WORD);
            }
            if (buffer.empty()) {
                continue;
            }
            if (buffer == CLOSE_KEY_WORD) {
                break;
            }
            std::stringstream stringStream;
            int index;
            std::string next;
            std::string blockName;
            std::string keyWord;
            std::vector<std::string> args;
            stringStream << buffer;

            next = stringStream.peek();
            if (!isdigit(next[0])) {
                stringStream >> next;
                throw exceptions::UnexpectedSymbol(next, BLOCK_ID);
            }
            stringStream >> index;
            stringStream >> keyWord;
            if (keyWord != BLOCK_DEF_KEY_WORD) {
                throw exceptions::UnexpectedSymbol(keyWord, BLOCK_DEF_KEY_WORD);
            }
            stringStream >> blockName;
            while (!stringStream.eof()) {
                std::string argsBuffer;
                stringStream >> argsBuffer;
                if (!argsBuffer.empty()) {
                    args.push_back(std::move(argsBuffer));
                }
            }
            addBlock(index, blockName, args);
        }
    }

    IChainParser &Parser::getChainParser() {
        chainParser->parseIndex();
        return *chainParser;
    }

    Parser::~Parser() {
        delete chainParser;
    }


}
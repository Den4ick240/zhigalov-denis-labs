package com.zhigalovLabs.Calculator.commandsWithLogger;

import com.zhigalovLabs.Calculator.CommandContext;
import com.zhigalovLabs.Calculator.commands.ICommand;
import com.zhigalovLabs.Calculator.exceptions.CalculatorException;

public class PushCommand extends CommandArgumentSetter implements ICommand {
    public PushCommand() {
        super(1);
    }

    @Override
    public void run(CommandContext context) throws CalculatorException {
        double val;
        try {
            val = Double.parseDouble(getArgs()[1]);
        } catch (NumberFormatException e) {
            val = context.getParameter(getArgs()[1]);
        }
        logger.info("Value pushed to stack: " + Double.toString(val));
        context.stackPush(val);
    }
}

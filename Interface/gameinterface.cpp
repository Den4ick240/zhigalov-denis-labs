#include "GameInterface.h"


namespace qt_interface {

    GameInterface::GameInterface(QGraphicsView *view)
        : _view(view), _loop(new QEventLoop()) {
    }

    std::string GameInterface::choosePlayerType(const std::set<std::string> &playerTypesSet, const std::string &playerName)
    {
        SelectionMenu menu(playerTypesSet, playerName);
        _view->setScene(&menu);
        _view->show();

        waitFor(&menu, SIGNAL(optionSelected()));

        return menu.getSelectedOption();
    }

    sea_battle::Ship GameInterface::getNextShipToPlace(const std::map<int, int> &unplacedShips)
    {
        _shipPlacingScene->refreshButtons();
        waitFor(_shipPlacingScene, SIGNAL(placeSelected()));

        return _shipPlacingScene->getShip();
    }

    sea_battle::IGameInterface::MoveStruct GameInterface::getNextMove(const sea_battle::OwnField &ownField, const sea_battle::EnemyField &enemyField)
    {

        waitFor(_battleScene, SIGNAL(moveDoneSignal()));
        return _battleScene->getMove();
    }

    void GameInterface::setView(QGraphicsView *view)
    {
        _view = view;
    }

    void GameInterface::setPlacingScene(ShipPlacingScene *scene)
    {
        _shipPlacingScene = scene;
    }

    void GameInterface::setBattleScene(BattleScene *scene)
    {
        _battleScene = scene;
    }

    void GameInterface::waitFor(QObject *sender, const char *signal)
    {
        connect(sender, signal, _loop, SLOT(quit()));
        _loop->exec();
        disconnect(sender, signal, _loop, SLOT(quit()));
    }


}

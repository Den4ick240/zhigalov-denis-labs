package com.zhigalovLabs.Calculator;

public interface IParser {
    public String[] split(String inputline);
}

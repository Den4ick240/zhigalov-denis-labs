#pragma once
#include "TritSetData.h"


namespace tritset
{
	class TritSetReference
	{
	public:
		TritSetReference& operator=(const Trit val) noexcept;
		TritSetReference& operator=(const TritSetReference & val) noexcept;
		operator Trit() const;

	private:
		TritSetReference(TritSetData & data_ptr, size_t pos);

		TritSetData & data;			//pointer to the data of tritset which created the reference
		size_t pos;					//position of the referenced trit in the tritset

		friend class TritSet;
	};
}
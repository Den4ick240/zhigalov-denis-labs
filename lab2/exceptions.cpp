#include <iostream>
#include "exceptions.h"

namespace workflow {
    namespace exceptions {
        const char *WorkFlowException::what() const _GLIBCXX_USE_NOEXCEPT {
            return message.c_str();
        }

        UnexpectedSymbol::UnexpectedSymbol(const std::string &found, const std::string &expected) {
            message = "Unexpected symbol: " + found + " was found, when " + expected + " was expected";
        }

        BlockDefinition::BlockDefinition(int index) {
            message = "Block definition error in block with index " + std::to_string(index);
        }

        BlockType::BlockType(int index, const std::string &type)
                : BlockDefinition(index) {
            message += ": Unknown block type: " + type;
        }

        ArgsAmount::ArgsAmount(int index, std::string type, int neededAmount, int actualAmount)
                : BlockDefinition(index) {
            message += "\nWrong amount of arguments, " +
                       std::to_string(actualAmount) + " " + (actualAmount == 1 ? "argument was" : "arguments were") +
                       " found, while " +
                       std::to_string(neededAmount) + " " + (neededAmount == 1 ? "argument was" : "arguments were") +
                       " expected for type " + type;
        }

        IndexReading::IndexReading() {
            message = "Couldn't read index from index chain";
        }

        NoIOFile::NoIOFile(NoIOFile::Type type) {
            message = "No ";
            if (type == input) {
                message += "input";
            }
            else {
                message += "output";
            }
            message += " file provided to workflow.";
        }

        UndefinedId::UndefinedId(int id) {
            message = "Undefined id in a block chain: " + std::to_string(id);
        }

        InvalidChainOrder::InvalidChainOrder() {
            message = "Wrong id chain: reader can be only in the beginning and writer only in the end";
        }

        RepeatedId::RepeatedId(int id) {
            message = "Repeated id in the block list : " + std::to_string(id);
        }

        InputFileNotFound::InputFileNotFound(const std::string &fileName) {
            message = "Readfile block could not open file " + fileName;
        }

        WorkflowFileNotFound::WorkflowFileNotFound(const std::string &fileName) {
            message = "Could not open file " + fileName;
        }
    }
}
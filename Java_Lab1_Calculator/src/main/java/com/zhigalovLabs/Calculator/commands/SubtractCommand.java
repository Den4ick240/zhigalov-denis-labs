package com.zhigalovLabs.Calculator.commands;

public class SubtractCommand extends BinaryOperationCommand {
    public SubtractCommand() {
        super(0);
    }

    @Override
    public double operation(double a, double b) {
        return a - b;
    }
}

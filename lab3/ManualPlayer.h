#pragma once

#include "IPlayer.h"
#include "IGameInterface.h"

namespace sea_battle {
    class ManualPlayer : public IPlayer {
    public:
        explicit ManualPlayer(OwnField &ownField, EnemyField &enemyField, IGameInterface &interface);

        bool move() override;

        void placeShips() override;

    private:
        IGameInterface &_interface;
    };
}
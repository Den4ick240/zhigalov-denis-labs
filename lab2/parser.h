#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
#include <list>
#include <iostream>

#include "parserInterface.h"
#include "chainParser.h"

#pragma once


namespace workflow {


    class Parser : public IParser {
    public:

        explicit Parser(std::istream &fileStream);

        ~Parser() override;

        void ParseBlocks(std::function<void(const int &, const std::string &,
                                            const std::vector<std::string> &)> addBlock) override;

        IChainParser &getChainParser() override;

    private:
        ChainParser *chainParser;
        std::istream &inputStream;
    };
}

#pragma once

#include "IPlayer.h"
#include <random>
#include <set>

namespace sea_battle {
    class RandomBotPlayer : public IPlayer {
    public:
        RandomBotPlayer(OwnField &ownField, EnemyField &enemyField);

        void placeShips() override;

        bool move() override;

    protected:
        std::mt19937 rand;
        std::vector<std::pair<int, int>> _freeCells;

    };
}

package com.zhigalovLabs.Calculator.commands;

import com.zhigalovLabs.Calculator.CommandContext;

import java.util.EmptyStackException;

public class SqrtCommand extends CommandArgumentSetter implements ICommand {
    public SqrtCommand() {
        super(0);
    }

    @Override
    public void run(CommandContext context) throws EmptyStackException {
        double result;
        result = context.stackPop();
        result = Math.sqrt(result);
        context.stackPush(result);
    }
}

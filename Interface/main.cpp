#include "mainwindow.h"

#include <QApplication>

#include "GameQt.h"
#include "GameInterface.h"
#include "InterfaceConsts.h"
#include "SelectionMenu.h"
#include "ClickableField.h"
#include "../lab3/RandomBotPlayer.h"
#include "ShipPlacingScene.h"


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QGraphicsView *view = new QGraphicsView();
    qt_interface::GameQt game(view);
    view->show();
    game.run();

    return a.exec();
}



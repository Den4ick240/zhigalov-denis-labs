package com.zhigalovLabs.Calculator.factory;

import com.zhigalovLabs.Calculator.Main;
import com.zhigalovLabs.Calculator.exceptions.CommandLoading;
import com.zhigalovLabs.Calculator.exceptions.ConfigFileException;
import com.zhigalovLabs.Calculator.exceptions.ConfigFileFormat;
import com.zhigalovLabs.Calculator.exceptions.ConfigFileReading;
import javafx.util.Pair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConfigReader {

    private final Logger logger = Main.getLogger();
    static private final String SEPARATOR = " ";
    static private final int EXPECTED_WORDS_NUMBER = 2;
    static private final int COMMAND_NAME_WORD_NUMBER = 0;
    static private final int COMMAND_CLASS_WORD_NUMBER = 1;
    private InputStream configStream;

    public ConfigReader(InputStream configStream) {
        this.configStream = configStream;
    }

    public Map<String, Class> readCommands() throws ConfigFileException {
        Map<String, Class> map = new HashMap<>();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(configStream))) {
            String line;
            while ((line = reader.readLine()) != null) {
                try {
                    var pair = parseLine(line);
                    map.put(pair.getKey(), pair.getValue());
                    logger.info("Class loaded: " + pair.getKey());
                } catch (ClassNotFoundException e) {
                    logger.log(Level.WARNING, "", e);
                    throw new CommandLoading(line);
                } catch (ConfigFileFormat e) {
                    System.out.println(e.getMessage());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            logger.log(Level.SEVERE, "Couldn't read config file", e);
            throw new ConfigFileReading();
        }
        return map;
    }

    private Pair<String, Class> parseLine(String line) throws ClassNotFoundException, ConfigFileFormat {
        String[] words = line.split(SEPARATOR);
        if (words.length != EXPECTED_WORDS_NUMBER) {
            logger.warning("invalid line format: " + line);
            throw new ConfigFileFormat(line);
        }
        String commandName = words[COMMAND_NAME_WORD_NUMBER];
        Class commandClass = Class.forName(words[COMMAND_CLASS_WORD_NUMBER]);
        return new Pair<>(commandName, commandClass);
    }

}

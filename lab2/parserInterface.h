#include <string>
#include <vector>
#include <functional>
#include <map>

#include "worker.h"

#pragma once

namespace workflow {

    class IChainParser {
    public:
        virtual bool is_finished() = 0;

        virtual IChainParser &operator++() = 0;

        virtual int operator*() = 0;
    };

    class IParser {
    public:
        virtual ~IParser() = default;

        virtual void ParseBlocks(
                std::function<void(const int &, const std::string &, const std::vector<std::string> &)> addBlock) = 0;

        virtual IChainParser &getChainParser() = 0;
    };

}
#pragma once
#include "InterfaceConsts.h"

namespace qt_interface {

    class GameScene : public QGraphicsScene {
    public:
        GameScene() {
            setSceneRect(0, 0, qt_interface::VIEW_WIDTH, qt_interface::VIEW_HEIGH);
            setBackgroundBrush(BACKGROUND_BRUSH);
        }
    };

}

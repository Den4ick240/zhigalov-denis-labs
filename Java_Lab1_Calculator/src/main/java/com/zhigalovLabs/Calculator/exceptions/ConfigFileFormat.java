package com.zhigalovLabs.Calculator.exceptions;

public class ConfigFileFormat extends ConfigFileException {
    final String name;

    public ConfigFileFormat(String name) {
        this.name = name;
    }

    @Override
    public String getMessage() {
        return "Invalid config file format: " + name;
    }
}

#pragma once
#include <map>

namespace tritset
{
	enum Trit { True = 3, False = 0, Unknown = 1 };
	typedef unsigned int unit;
	const unit UNKNOWN_UNIT = 0x5555'5555'5555'5555;	//5 is byte mask for unknown - 01'01
	const unit TWO_BITS_TRUE_MASK = 3;
	const std::map <Trit, char> INIT_VALUES_FOR_UNITS =
	{
		{ True, ~0 },
		{ Unknown, UNKNOWN_UNIT },
		{ False, 0 }
	};
	class TritSet;
	class TritSetReference;
}
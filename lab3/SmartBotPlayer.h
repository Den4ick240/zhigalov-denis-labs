#pragma once
#include "Consts.h"

#include "IPlayer.h"
#include <random>
#include <exception>

namespace sea_battle {

    class InvalidShipAmountInfo : public std::exception {
    public:
        const char * what() const throw() override;
    };

    class SmartBotPlayer : public IPlayer {
    public:
        SmartBotPlayer(OwnField &ownField, EnemyField &enemyField);

        void placeShips() override;

        bool move() override;

    private:
        enum Direction { horizontal, vertical, unknown } _direction = unknown;
        std::map<int, int> _enemyShipsLeft;
        bool _finishingOff = false;
        int _last_move_x;
        int _last_move_y;
        int _gridOffset;
        int _gridSize;
        std::mt19937 rand;

        bool finishOff();

        std::pair<int, int> choseFinishingShootCell(Direction, int x, int y);

        bool fitShip(Direction, int x, int y, int size);

        int excludeCellsAroundShip(int x, int y);

        int excludeRecursively(int center_x, int center_y, int previous_x, int previous_y, int killedShipSize = 0);

        int biggestShipSize();

        bool randomBool();

    };

}

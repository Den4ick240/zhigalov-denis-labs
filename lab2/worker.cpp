#include "worker.h"
#include <fstream>
#include <iostream>
#include <algorithm>
#include <utility>

#include "exceptions.h"

namespace workflow {

    IBlock::IBlock(BlockType blockType)
            : blockType(blockType) {
    }

    Readfile::Readfile(std::string fileName)
            : IBlock(BlockType::input), fileName(std::move(fileName)) {
    }

    void Readfile::DoWork(std::vector<std::string> &text) {
        std::ifstream file;
        std::string buff;

        file.open(fileName);

        if (!file.is_open()) {
            throw exceptions::InputFileNotFound(fileName);
        }

        while (!file.eof()) {
            std::getline(file, buff);
            text.push_back(std::move(buff));
        }

        file.close();
    }

    Writefile::Writefile(std::string fileName)
            : IBlock(BlockType::output), fileName(std::move(fileName)) {
    }

    void Writefile::DoWork(std::vector<std::string> &text) {
        std::ofstream file;

        file.open(fileName);
        for (const auto &str : text) {
            file << str << std::endl;
        }
        file.close();

    }

    Grep::Grep(std::string word)
            : IBlock(BlockType::worker), word(std::move(word)) {
    }

    void Grep::DoWork(std::vector<std::string> &text) {
        auto *dist = new std::vector<std::string>();

        for (const auto &str : text) {
            if (str.find(word) != std::string::npos) {
                dist->push_back(str);
            }
        }
    }


    Sort::Sort()
            : IBlock(BlockType::worker) {
    }

    void Sort::DoWork(std::vector<std::string> &text) {
        std::sort(text.begin(), text.end());
    }


    Replace::Replace(std::string word1, std::string word2)
            : IBlock(BlockType::worker), word1(std::move(word1)), word2(std::move(word2)) {}

    void Replace::DoWork(std::vector<std::string> &text) {
        for (auto &str : text) {
            size_t pos = 0;
            while ((pos = str.find(word1, pos + word1.size())) != std::string::npos) {
                str.replace(pos, word1.size(), word2);
            }
        }
    }

    Dump::Dump(std::string fileName)
            : IBlock(BlockType::worker), fileName(std::move(fileName)) {
    }

    void Dump::DoWork(std::vector<std::string> &text) {
        std::ofstream file;

        file.open(fileName);
        for (const auto &str : text) {
            file << str << std::endl;
        }
        file.close();
    }
}
#include "SmartBotPlayer.h"
#include <ctime>
#include <cassert>

namespace sea_battle {

    SmartBotPlayer::SmartBotPlayer(OwnField &ownField, EnemyField &enemyField)
            : IPlayer(ownField, enemyField), _enemyShipsLeft(SHIP_SIZES), rand(clock()) {
        _gridSize = biggestShipSize();
        _gridOffset = rand() % _gridSize;
    }

    void SmartBotPlayer::placeShips() {
        auto &unplacedShips = _ownField.getUnplacedShips();

        while (unplacedShips.size() > 1) {
            int size = rand() % 3 + 2;
            Ship ship(size);
            Ship::Orientation orientation = (randomBool() ? Ship::Orientation::vertical
                                                          : Ship::Orientation::horizontal);
            if (orientation == Ship::Orientation::vertical) {
                int x = (FIELD_WIDTH - 1) * (rand() % 2);
                int y = rand() % (FIELD_HEIGHT - size);
                ship.move(x, y);
            } else {
                int y = (FIELD_HEIGHT - 1) * (rand() % 2);
                int x = rand() % (FIELD_WIDTH - size);
                ship.move(x, y);
            }
            if (ship.orientation() != orientation) {
                ship.rotate();
            }
            _ownField.tryToAddShip(ship);
        }

        while (!unplacedShips.empty()) {
            Ship ship(1);
            ship.move(rand() % FIELD_WIDTH, rand() % FIELD_HEIGHT);
            _ownField.tryToAddShip(ship);
        }
    }

    bool SmartBotPlayer::move() {
        if (biggestShipSize() < _gridSize && _gridSize > 1) {
            _gridSize /= 2;
        }

        if (!_finishingOff) {
            std::vector<std::pair<int, int>> freeCells;
            for (int y = 0; y < FIELD_HEIGHT; y++) {
                for (int x = (y + _gridOffset) % _gridSize; x < FIELD_WIDTH; x += _gridSize) {
                    if (_enemyField.getCell(x, y) == CellState::clear) {
                        freeCells.emplace_back(x, y);
                    }
                }
            }

            if (freeCells.empty()) {
                throw InvalidShipAmountInfo();
            }

            auto[x, y] = freeCells[rand() % freeCells.size()];
            bool shotResult = _enemyField.shot(x, y);
            auto cellState = _enemyField.getCell(x, y);
            if (cellState == CellState::hit || cellState == CellState::dead) {
                _finishingOff = true;
                _last_move_x = x;
                _last_move_y = y;
            }
            return shotResult;
        } else {
            return finishOff();
        }
    }

    bool validCell(int x, int y) {
        return x < FIELD_WIDTH && y < FIELD_HEIGHT && x >= 0 && y >= 0;
    }

    bool SmartBotPlayer::finishOff() {
        if (_enemyField.getCell(_last_move_x, _last_move_y) == CellState::dead) {
            _finishingOff = false;
            _direction = unknown;

            int shipSize = excludeCellsAroundShip(_last_move_x, _last_move_y);
            auto iter = _enemyShipsLeft.find(shipSize);

            if (iter == _enemyShipsLeft.end()) {
                throw InvalidShipAmountInfo();
            }

            iter->second--;
            if (iter->second == 0) {
                _enemyShipsLeft.erase(iter);
            }
            return true;
        }

        assert(_enemyField.getCell(_last_move_x, _last_move_y) == CellState::hit);

        int minShipSizeAboveOne;
        if (_enemyShipsLeft.begin()->first == 1) {
            minShipSizeAboveOne = (++_enemyShipsLeft.begin())->first;
        } else {
            minShipSizeAboveOne = _enemyShipsLeft.begin()->first;
        }

        if (_direction == unknown) {
            if (!fitShip(horizontal, _last_move_x, _last_move_y, minShipSizeAboveOne)) {
                _direction = vertical;
            } else if (!fitShip(vertical, _last_move_x, _last_move_y, minShipSizeAboveOne)) {
                _direction = horizontal;
            } else {                      //if both directions can fit a ship, then chose randomly, and see if we hit anything
                _direction = (randomBool() % 2 ? vertical : horizontal);
                auto[x, y] = choseFinishingShootCell(_direction, _last_move_x, _last_move_y);
                bool shootingResult = _enemyField.shot(x, y);

                if (!shootingResult) {
                    _direction = unknown;
                }
                return shootingResult;
            }
        }
        auto[x, y] = choseFinishingShootCell(_direction, _last_move_x, _last_move_y);

        return _enemyField.shot(x, y);
    }

    std::pair<int, int> SmartBotPlayer::choseFinishingShootCell(SmartBotPlayer::Direction dir, int cell_x, int cell_y) {
        int x, y;
        int *changingCoord;
        if (dir == vertical) {
            changingCoord = &y;
        } else {
            changingCoord = &x;
        }

        int shift = (randomBool() ? 1 : -1);
        for (int i = 0; i < 2; i++) {
            x = cell_x;
            y = cell_y;
            while (validCell(x, y) && _enemyField.getCell(x, y) == CellState::hit) {
                *changingCoord += shift;
            }
            if (validCell(x, y) && _enemyField.getCell(x, y) == CellState::clear) {
                return std::pair<int, int>(x, y);
            }
            shift = -shift;
        }
        throw std::logic_error("Free cells not found");
    }

    bool SmartBotPlayer::fitShip(SmartBotPlayer::Direction dir, int cell_x, int cell_y, int size) {
        assert(dir != unknown);

        int x, y;
        int *changingCoord;
        int clearCellsNum = 1;
        if (dir == vertical) {
            changingCoord = &y;
        } else {
            changingCoord = &x;
        }

        for (int shift = -1; shift <= 1; shift += 2) {
            x = cell_x;
            y = cell_y;
            *changingCoord += shift;
            while (validCell(x, y) && _enemyField.getCell(x, y) == CellState::clear) {
                *changingCoord += shift;
                clearCellsNum++;
            }
        }
        return size <= clearCellsNum;
    }


    int SmartBotPlayer::excludeCellsAroundShip(int x, int y) {
        return excludeRecursively(x, y, x, y);
    }

    int
    SmartBotPlayer::excludeRecursively(int center_x, int center_y, int previous_x, int previous_y, int killedShipSize) {
        if (_enemyField.getCell(center_x, center_y) != CellState::dead) {
            return killedShipSize;
        }
        killedShipSize++;
        for (int y = center_y - 1; y <= center_y + 1; y++) {
            for (int x = center_x - 1; x <= center_x + 1; x++) {
                if (!validCell(x, y)) {
                    continue;
                }
                if (x == previous_x && y == previous_y) {
                    continue;
                }
                if (x == center_x && y == center_y) {
                    continue;
                }
                if (_enemyField.getCell(x, y) == CellState::dead) {
                    killedShipSize = excludeRecursively(x, y, center_x, center_y, killedShipSize);
                }
                _enemyField.tryExclude(x, y);
            }
        }
        return killedShipSize;

    }

    bool SmartBotPlayer::randomBool() {
        return int(rand()) % 2 == 0;
    }

    int SmartBotPlayer::biggestShipSize() {
        return (--_enemyShipsLeft.end())->first;
    }

    const char *InvalidShipAmountInfo::what() const throw() {
        return "information about enemy ships provided to the bot does not correspond to reality";
    }

}
